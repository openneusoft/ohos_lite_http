package com.litesuits.http_sample.slice;

import com.alibaba.fastjson.JSONObject;
import com.litesuits.go.OverloadPolicy;
import com.litesuits.go.SchedulePolicy;
import com.litesuits.go.SmartExecutor;
import com.litesuits.http.HttpConfig;
import com.litesuits.http.LiteHttp;
import com.litesuits.http.annotation.*;
import com.litesuits.http.data.*;
import com.litesuits.http.request.*;
import com.litesuits.http.request.content.*;
import com.litesuits.http.request.content.multi.*;
import com.litesuits.http.request.param.*;
import com.litesuits.http.exception.HttpException;
import com.litesuits.http.impl.huc.HttpUrlClient;
import com.litesuits.http_sample.MainAbility;
import com.litesuits.http_sample.ResourceTable;
import com.litesuits.http_sample.base.BaseAbilitySlice;
import com.litesuits.http_sample.butterknife.Bind;
import com.litesuits.http_sample.custom.CustomJSONParser;
import com.litesuits.http_sample.custom.MyHttpExceptHandler;
import com.litesuits.http_sample.model.User;
import com.litesuits.http_sample.model.UserParam;
import com.litesuits.http.listener.GlobalHttpListener;
import com.litesuits.http.listener.HttpListener;
import com.litesuits.http.log.HttpLog;
import com.litesuits.http.request.query.JsonQueryBuilder;
import com.litesuits.http.response.Response;
import com.litesuits.http.utils.HttpUtil;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import ohos.agp.text.Layout;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ListDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Environment;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.miscservices.timeutility.Time;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/**
 * MainAbilitySlice main class
 */
public class MainAbilitySlice extends BaseAbilitySlice implements Component.ClickedListener {
    private static final int COLOR_BLACK = 0xffff0000;
    private static final int COLOR_RED = 0xff000000;

    private static ListDialog listDialog;

    @Bind(ResourceTable.Id_posttest_list)
    private Layout posttestList;

    private Button[] tcIndicator = new Button[25];
    @Bind(ResourceTable.Id_tc_indicator0)
    private Button tcIndicator0;
    @Bind(ResourceTable.Id_tc_indicator1)
    private Button tcIndicator1;
    @Bind(ResourceTable.Id_tc_indicator2)
    private Button tcIndicator2;
    @Bind(ResourceTable.Id_tc_indicator3)
    private Button tcIndicator3;
    @Bind(ResourceTable.Id_tc_indicator4)
    private Button tcIndicator4;
    @Bind(ResourceTable.Id_tc_indicator5)
    private Button tcIndicator5;
    @Bind(ResourceTable.Id_tc_indicator6)
    private Button tcIndicator6;
    @Bind(ResourceTable.Id_tc_indicator7)
    private Button tcIndicator7;
    @Bind(ResourceTable.Id_tc_indicator8)
    private Button tcIndicator8;
    @Bind(ResourceTable.Id_tc_indicator9)
    private Button tcIndicator9;
    @Bind(ResourceTable.Id_tc_indicator10)
    private Button tcIndicator10;
    @Bind(ResourceTable.Id_tc_indicator11)
    private Button tcIndicator11;
    @Bind(ResourceTable.Id_tc_indicator12)
    private Button tcIndicator12;
    @Bind(ResourceTable.Id_tc_indicator13)
    private Button tcIndicator13;
    @Bind(ResourceTable.Id_tc_indicator14)
    private Button tcIndicator14;
    @Bind(ResourceTable.Id_tc_indicator15)
    private Button tcIndicator15;
    @Bind(ResourceTable.Id_tc_indicator16)
    private Button tcIndicator16;
    @Bind(ResourceTable.Id_tc_indicator17)
    private Button tcIndicator17;
    @Bind(ResourceTable.Id_tc_indicator18)
    private Button tcIndicator18;
    @Bind(ResourceTable.Id_tc_indicator19)
    private Button tcIndicator19;
    @Bind(ResourceTable.Id_tc_indicator20)
    private Button tcIndicator20;
    @Bind(ResourceTable.Id_tc_indicator21)
    private Button tcIndicator21;
    @Bind(ResourceTable.Id_tc_indicator22)
    private Button tcIndicator22;
    @Bind(ResourceTable.Id_tc_indicator23)
    private Button tcIndicator23;
    @Bind(ResourceTable.Id_tc_indicator24)
    private Button tcIndicator24;

    protected String TAG = MainAbilitySlice.class.getSimpleName();
    private int categoryId = 0;
    private boolean needRestore;
    protected static LiteHttp liteHttp;
    protected Ability activity = null;

    private LayoutScatter scatter = null;
    private ComponentContainer processBar = null;
    private CommonDialog commonDialog = null;
    private ProgressBar upProgress = null;

    public static String absolutePath = "/storage/emulated/0/Android/data/litesuits.http";
    public static String url = "http://baidu.com";
    public static final String httpsUrl = "https://www.baidu.com";
    // 跳转hao123
    public static final String redirectUrl = "https://www.baidu.com/link?url=Lqc3GptP8u05JCRDsk0jqsAvIZh9WdtO_RkXYMYRQEm";

    // 虚拟机测试User
//    public static final String baseUrl = "https://getman.cn";
    // 虚拟机测试图片
    public static final String baseUrl = "https://img2.baidu.com/it";
    // 虚拟机
    public static final String userUrl = "/echo";
    public static final String uploadUrl = "https://getman.cn/echo";
    public static final String loginUrl = "https://getman.cn/echo/user_get";
    public static final String picUrl = "/u=1725422096,2181202425&fm=26&fmt=auto&gp=0.jpg";

    // 真机测试
//    public static final String baseUrl = "http://192.168.1.156:8080";
//    public static final String userUrl = "/user_get";
//    public static final String loginUrl = "http://192.168.1.156:8080/user_get";
//    public static final String uploadUrl = "/upload";
//    public static final String picUrl = "/1.jpg";

    private EventHandler mainHandler;

    public MainAbilitySlice() {
    }

    @Override
    public int getLayout() {
        return ResourceTable.Layout_product_list;
    }

    @Override
    protected void initWidget() {
        //absolutePath = getContext().getExternalCacheDir().getAbsolutePath();
        super.initWidget();
        initList();
        initListener();
        mainHandler = new EventHandler(EventRunner.getMainEventRunner());
        scatter = LayoutScatter.getInstance(this);
        processBar = (ComponentContainer) scatter.parse(ResourceTable.Layout_processBar_dialog, null, true);
        commonDialog = new CommonDialog(this);
        upProgress = (ProgressBar) findComponentById(ResourceTable.Id_progressbar);
    }

    private void clickTestItem(final int which) {
        // restore http config
        if (needRestore) {
            liteHttp.getConfig().restoreToDefault().setBaseUrl(baseUrl);
            needRestore = false;
        }
        switch (which) {
            case 0:
                initLiteHttp();
                liteHttp.getConfig()                   // configuration directly
                        .setSocketTimeout(1000)       // socket timeout: 5s
                        .setConnectTimeout(1000);    // connect timeout: 5s
                HttpUtil.showTips(getContext(), "LiteHttp2.0", "Init Config Success!");

                String apiUrl = "http://api.daily.taobao.net/router/rest?app_key=4272&format=json&method=cainiao.guoguo.courier.getrewardtip&open_id=4398046618004&session_code=7949690a30a91305a3a351a314ec501e&sign=0A9DDED897FDB04E1D8177C984AD4238&sign_method=md5&timestamp=2016-08-05&user_id=4398046618004&v=2.0";
                //https
                //String apiUrl = baseUrl + userUrl + "?id=1&key=esc";
                StringRequest login = new StringRequest(apiUrl)
                    .setMethod(HttpMethods.Get)
                    .setHttpListener(new HttpListener<String>() {
                        @Override
                        public void onSuccess(String s, Response<String> response) {
                            response.printInfo();
                            mainHandler.postTask(() ->
                            HttpUtil.showTips(getContext(), "LiteHttp2.0", s));
                        }
                        @Override
                        public void onFailure(HttpException e, Response<String> response) {
                            response.printInfo();
                            mainHandler.postTask(() ->
                            HttpUtil.showTips(getContext(), "LiteHttp2.0", "Failure:" + e.getMessage()));
                        }
                    });
                liteHttp.executeAsync(login);
                break;

            case 1:
                // 1. Asynchronous Request
                // 1.0 init request
                final StringRequest request = new StringRequest(/*url*/ "http://www.baidu.com").setHttpListener(
                        new HttpListener<String>() {

                            @Override
                            public void onSuccess(String s, Response<String> response) {
                                mainHandler.postTask(() ->
                                HttpUtil.showTips(getContext(), "S:Asynchronous", s));
                                response.printInfo();
                            }

                            @Override
                            public void onFailure(HttpException e, Response<String> response) {
                                mainHandler.postTask(() ->
                                HttpUtil.showTips(getContext(), "F:Asynchronous", e.toString()));
                            }
                        }
                );
                liteHttp.executeAsync(request);

                // 1.2 perform async, future task returned.
                final StringRequest request2 = new StringRequest(url).setHttpListener(
                        new HttpListener<String>() {
                            @Override
                            public void onCancel(String s, Response<String> response) {
                                mainHandler.postTask(() ->
                                HttpUtil.showTips(getContext(), "Perform Async", "Async Task Cancel"));
                            }
                        }
                );
                // get future task
                FutureTask<String> task = liteHttp.performAsync(request2);
                // cancel request
                request2.cancel();

                break;
            case 2:
                // 2. Synchronous Request
                Runnable run = new Runnable() {
                    @Override
                    public void run() {
                        // 2.0 execute: return fully response
                        Response<User> response = liteHttp.execute(new LoginParam("a", "a"));
                        User user = response.getResult();
                        HttpLog.i(TAG, "User: " + user);
                        mainHandler.postTask(() -> HttpUtil.showTips(getContext(), "RichParam", "Success"));
//                        HttpUtil.showTips(getContext(), "RichParam", "Success");

                        // 2.1 perform: return java model directly
                        User user2 = liteHttp.perform(new JsonAbsRequest<User>(userUrl) {});
                        HttpLog.i(TAG, "User: " + user2);
                        mainHandler.postTask(() -> HttpUtil.showTips(getContext(), "JsonAbsRequest", "User: " + user2));
//                        HttpUtil.showTips(getContext(), "JsonAbsRequest", "User: " + user2);

                        // 2.2 return data directly, handle result on current thread
                        PixelMap bitmap = liteHttp.perform(new BitmapRequest(picUrl).setHttpListener(
                                new HttpListener<PixelMap>(false, true, true) {

                                    @Override
                                    public void onSuccess(PixelMap pixelMap, Response<PixelMap> response) {
                                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "S:Synchronous", "Success"));
//                                      HttpUtil.showTips(getContext(), "S:Synchronous", "Success");
                                    }

                                    @Override
                                    public void onFailure(HttpException e, Response<PixelMap> response) {
                                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "F:Synchronous", "Fail"));
//                                        HttpUtil.showTips(getContext(), "F:Synchronous", "Fail");
                                    }

                                    @Override
                                    public void onLoading(AbstractRequest<PixelMap> request, long total, long len) {
                                        // down loading notification ...
                                        HttpLog.i(TAG, "total: " + total + "  len: " + len);
                                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "S:Synchronous", "total: " + total + "  len: " + len));
//                                        HttpUtil.showTips(getContext(), "S:Synchronous", "total: " + total + "  len: " + len);
                                    }
                                }));
                        if (bitmap != null) {
                            bitmap.release();
                        }

                        // 2.3 handle result on UI thread(主线程处理，注意HttpListener默认是在主线程回调)
                        liteHttp.execute(new StringRequest(url).setHttpListener(
                                new HttpListener<String>(true) {
                                    @Override
                                    public void onSuccess(String data, Response<String> response) {
                                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "S:Synchronous", data));
//                                        HttpUtil.showTips(getContext(), "S:Synchronous", data);
                                    }

                                    @Override
                                    public void onFailure(HttpException e, Response<String> response) {
                                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "F:Synchronous", e.getMessage()));
//                                        HttpUtil.showTips(getContext(), "F:Synchronous", e.getMessage());
                                    }
                                }
                        ));
                    }
                };
                new Thread(run).start();
                break;
            case 3:
                // 3. Simple Synchronous Request
                run = new Runnable() {
                    @Override
                    public void run() {
                        // 3.0 simple get
                        String result = liteHttp.get(url);
                        HiLog.info(new HiLogLabel(HiLog.LOG_APP, 0x00101, TAG), "Simple Get String: \n" + result);
                        //HttpUtil.showTips(getContext(), "Simple Synchronous Request", " Simple Get String: " + result);
                        // 3.1 simple post
                        result = liteHttp.post(new StringRequest(httpsUrl));
                        HiLog.info(new HiLogLabel(HiLog.LOG_APP, 0x00101, TAG), "Simple POST String: \n" + result);
                        //HttpUtil.showTips(getContext(), "Simple Synchronous Request", " Simple POST String: " + result);

                        // 3.2 simple post and publish
                        User user = liteHttp.get(userUrl, User.class);
                        HiLog.info(new HiLogLabel(HiLog.LOG_APP, 0x00101, TAG), "Simple Get User: \n" + result);
                        //HttpUtil.showTips(getContext(), "Simple Synchronous Request", " Simple Get User: " + result);

                        // 3.3 simple head and return
                        ArrayList<NameValuePair> headers = liteHttp.head(new StringRequest(url));
                        HiLog.debug(new HiLogLabel(HiLog.LOG_APP, 0x00101, TAG), "Simple Mode: \n" + result);
                        //HttpUtil.showTips(getContext(), "Simple Synchronous Request", " Simple Mode: " + result);
                    }
                };
                Thread th=  new Thread(run);
                th.setDaemon(true);
                th.start();
                break;
            case 4:
                // 4. Exception Thrown Request
                run = new Runnable() {
                    @Override
                    public void run() {
                        // http scheme error
                        try {
                            Response response = liteHttp.executeOrThrow(new BytesRequest("abc://eee.com"));
                        } catch (HttpException e) {
                            //HttpUtil.showTips(getContext(), "BytesRequest", e.getLocalizedMessage());
                            HttpLog.e(TAG, "HttpException: " + e.getCause());
                            HttpLog.e(TAG, "HttpException: " + e.getMessage());
                            HttpLog.e(TAG, "HttpException: " + e.getLocalizedMessage());
                        }
                        // java model translate error
                        try {
                            User user = liteHttp.performOrThrow(new JsonAbsRequest<User>("http://thanku.love") {});
                        } catch (final Exception e) {
                            //HttpUtil.showTips(getContext(), "JsonAbsRequest", e.getLocalizedMessage());
                            if (mainHandler == null) {
                                mainHandler = new EventHandler(EventRunner.getMainEventRunner());
                            }

                            mainHandler.postTask(new Runnable() {
                                @Override
                                public void run() {
                                    //HttpUtil.showTips(getContext(), "Thrown Test", e.getMessage());
                                }
                            });

                        }

                    }
                };

                new Thread(run).start();

                break;
            case 5:
                // 5. HTTPS Reqeust
                liteHttp.executeAsync(new StringRequest(httpsUrl).setHttpListener(
                        new HttpListener<String>() {
                            @Override
                            public void onSuccess(String s, Response<String> response) {
                                mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "HTTPS TEST", " Read Content Length: " + s.length()));
//                                HttpUtil.showTips(getContext(), "HTTPS TEST", " Read Content Length: " + s.length());
                            }

                            @Override
                            public void onFailure(HttpException e, Response<String> response) {
                                mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "HTTPS TEST", Arrays.toString(e.getStackTrace())));
//                                HttpUtil.showTips(getContext(), "HTTPS TEST", Arrays.toString(e.getStackTrace()));
                            }
                        }
                ));
                break;
            case 6:
                // 6. Automatic Model Conversion
                // build as http://{userUrl}?id=168&key=md5
                liteHttp.executeAsync(new StringRequest(userUrl, new UserParam(18, "md5")).setHttpListener(
                    new HttpListener<String>() {
                        @Override
                        public void onSuccess(String data, Response<String> response) {
                            mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "S:HTTPS TEST", " Read Content: " + data));
//                            HttpUtil.showTips(getContext(), "S:HTTPS TEST", " Read Content: " + data);
                            HttpLog.i(TAG, "USER: " + data);
                        }

                        @Override
                        public void onFailure(HttpException e, Response<String> response) {
                            mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "F:HTTPS TEST", Arrays.toString(e.getStackTrace())));
//                            HttpUtil.showTips(getContext(), "F:HTTPS TEST", Arrays.toString(e.getStackTrace()));
                            HttpLog.e(TAG, "Exception: " + Arrays.toString(e.getStackTrace()));
                        }
                    }));

                /**
                 * Request Model : json string translate to user object
                 */
                class UserRequest extends JsonAbsRequest<User> {
                    public UserRequest(String url, HttpParamModel param) {
                        super(url, param);
                    }
                }
                // build as http://{userUrl}?id=168&key=md5
                UserRequest userRequest = new UserRequest(userUrl, new UserParam(18, "md5"));
                userRequest.setHttpListener(new HttpListener<User>() {
                    @Override
                    public void onSuccess(User user, Response<User> response) {
                        // data has been translated to user object
                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "S:Model Convert", user.toString()));
//                        HttpUtil.showTips(getContext(), "S:Model Convert", user.toString());
                    }

                    @Override
                    public void onFailure(HttpException e, Response<User> response) {
                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "F:HTTPS TEST", Arrays.toString(e.getStackTrace())));
//                        HttpUtil.showTips(getContext(), "F:HTTPS TEST", Arrays.toString(e.getStackTrace()));
                        HttpLog.e(TAG, "Exception: " + Arrays.toString(e.getStackTrace()));
                    }
                });
                liteHttp.executeAsync(userRequest);

                break;
            case 7:
                // 7. Custom Data Parser
                JsonRequest<JSONObject> jsonRequest = new JsonRequest<JSONObject>(userUrl, JSONObject.class);
                jsonRequest.setHttpBody(new JsonBody(new UserParam(168, "haha-key")));
                jsonRequest.setDataParser(new CustomJSONParser());
                liteHttp.executeAsync(jsonRequest.setHttpListener(new HttpListener<JSONObject>() {
                    @Override
                    public void onSuccess(JSONObject jsonObject, Response<JSONObject> response) {
                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "S:Custom Data Parser",
                                "Custom JSONObject Parser:\n" + jsonObject.toString()));
                    }

                    @Override
                    public void onFailure(HttpException e, Response<JSONObject> response) {
                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "F:Custom Data Parser",
                                "Custom JSONObject Parser:\n" + e.getLocalizedMessage()));
                    }
                }));
                break;
            case 8:
                // 8. Replace Json Convertor
                // first, set new json framework instance. then, over.
                liteHttp.getConfig().setJsonConvertor(new FastJson());
//                String uj = "{\"api\":\"com.xx.get.userinfo\",\"v\":\"1.0\",\"code\":200,\"message\":\"success\",\"data\":{\"age\":18,\"name\":\"qingtianzhu\",\"girl_friends\":[\"xiaoli\",\"fengjie\",\"lucy\"]}}";
//                User u11 = Json.get().toObject(uj, User.class);

                // json model convert used #FastJson
                liteHttp.executeAsync(new JsonAbsRequest<User>(userUrl) {}
                    .setHttpListener(new HttpListener<User>() {
                    @Override
                    public void onSuccess(User user, Response<User> response) {
                        response.printInfo();
                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "CASE8:Json Convertor", "FastJson handle this: \n" + user.toString()));
                        needRestore = true;
                    }

                    @Override
                    public void onFailure(HttpException e, Response<User> response) {
                    }
                }));
                // json model convert used #FastJson
                liteHttp.performAsync(new StringRequest(userUrl).setHttpListener(new HttpListener<String>() {
                    @Override
                    public void onSuccess(String s, Response<String> response) {
                        User u = Json.get().toObject(s, User.class);
                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "CASE8:Json Convertor", "FastJson handle this: \n" + u.toString()));
                        needRestore = true;
                    }

                    @Override
                    public void onFailure(HttpException e, Response<String> response) {
                    }

                    @Override
                    public void onEnd(Response<String> response) {
                        needRestore = true;
                    }
                }));
                break;
            case 9:
                // 9. File/Bitmap Download

                // 测试过程中用到图片时baseurl需要改成"https://img2.baidu.com/it"
                final ProgressBar downProgress = new ProgressBar(getContext());
                downProgress.setOrientation(Component.HORIZONTAL);
                downProgress.setIndeterminate(false);
                String picUrl1 = "/u=1725422096,2181202425&fm=26&fmt=auto&gp=0.jpg";
//                String picUrl1 = "/1.jpg";


                // load and show bitmap
                liteHttp.executeAsync(
                        new BitmapRequest(picUrl1).setHttpListener(new HttpListener<PixelMap>(true, true, false) {
                            @Override
                            public void onLoading(AbstractRequest<PixelMap> request, long total, long len) {

                                mainHandler.postTask(() ->
                                {
                                    downProgress.setMaxValue((int) total);
                                    downProgress.setProgressValue((int) len);
                                    HttpUtil.showTips(getContext(), "Loading", total + "  total   " + len + " len");
                                });
                            }

                            @Override
                            public void onSuccess(PixelMap bitmap, Response<PixelMap> response) {

                                mainHandler.postTask(() ->
                                {
                                    downProgress.release();
//                                getUITaskDispatcher().syncDispatch(()   -> {
                                    Image image = new Image(getContext());
                                    image.setHeight(500);
                                    image.setWidth(300);
                                    image.setMarginTop(20);
                                    image.setMarginLeft(20);
                                    image.setPixelMap(bitmap);
                                    image.setScaleMode(Image.ScaleMode.ZOOM_CENTER);
                                    CommonDialog b = (CommonDialog) HttpUtil.dialogBuilder(getContext(), "Bitmap Download", "");
                                    b.setContentCustomComponent(image);
                                    b.setDuration(2000);
                                    b.show();
                                });
                            }

                            @Override
                            public void onFailure(HttpException e, Response<PixelMap> response) {

                                mainHandler.postTask(() ->
                                {
                                    downProgress.release();
                                    HttpUtil.showTips(getContext(), "F:Bitmap Download", e.toString());
                                });
                            }
                        }));

                // download a file to externalCacheDir.
                FileRequest fileRequest = new FileRequest(picUrl1, absolutePath + "/1.jpg");
                liteHttp.executeAsync(fileRequest);
                liteHttp.executeAsync(new FileRequest(userUrl, "sdcard/user.txt"));
                break;
            case 10:
                // 10. File Upload
                mainHandler.postTask(() ->
                        {
                            commonDialog.setContentCustomComponent(processBar);
                            commonDialog.setTitleText("File Upload");
                            commonDialog.show();
                        });

                StringRequest uploadRequest = new StringRequest(uploadUrl);

                uploadRequest.setMethod(HttpMethods.Post)
                        .setHttpBody(new FileBody(new File(absolutePath + "/1.jpg")))
                        .setHttpListener(new HttpListener<String>(true, false, true) {
                            @Override
                            public void onSuccess(String s, Response<String> response) {
                                mainHandler.postTask(() ->
                                {
                                    HttpUtil.showTips(getContext(), "Upload Success", s);
                                    commonDialog.remove();
                                });
                                response.printInfo();
                            }

                            @Override
                            public void onFailure(HttpException e, Response<String> response) {
                                HiLog.debug(new HiLogLabel(HiLog.DEBUG, 0x00101, "yang.dan"), e.getMessage());
                                mainHandler.postTask(() ->
                                {
                                    HttpUtil.showTips(getContext(), "Upload Failed", e.getMessage());
                                    commonDialog.remove();
                                });
                            }

                            @Override
                            public void onUploading(AbstractRequest<String> request, long total, long len) {
                                mainHandler.postTask(() ->
                                {
                                    upProgress.setMaxValue((int) total);
                                    upProgress.setProgressValue((int) len);
                                });
                            }
                        });
                liteHttp.executeAsync(uploadRequest);
                break;
            case 11:
                // 11. Disable Some Network
                HttpConfig config = liteHttp.getConfig();
                // must set context
                config.setContext(getContext());
                // disable mobile(2G/3G/4G) and wifi network
                config.setDisableNetworkFlags(HttpConfig.FLAG_NET_DISABLE_MOBILE | HttpConfig.FLAG_NET_DISABLE_WIFI);
                url = "https://www.baidu.com";
                liteHttp.executeAsync(new StringRequest(url).setHttpListener(new HttpListener<String>() {
                    @Override
                    public void onSuccess(String s, Response<String> response) {
                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "Disable Network", s));
                    }

                    @Override
                    public void onFailure(HttpException e, Response<String> response) {
                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "Network Disabled", e.toString()));
                    }
                }));

                needRestore = true;
                break;

            case 12:
                // 12. Traffic/Time Statistics
                // turn on
                liteHttp.getConfig().setDoStatistics(true);
                // see detail
                liteHttp.executeAsync(new FileRequest(picUrl).setHttpListener(new HttpListener<File>() {
                    @Override
                    public void onSuccess(File file, Response<File> response) {
                        mainHandler.postTask(() ->
                        {
                            String msg = "This request take time:" + response.getUseTime()
                                    + ", readed length:" + response.getReadedLength();
                            msg += "  Global " + liteHttp.getStatisticsInfo();
                            HttpUtil.showTips(getContext(), "Traffic/Time Statistics", msg);
                        });

                        response.getRedirectTimes();  // 重定向的次数
                        response.getRetryTimes();     // 重试的次数
                        response.getUseTime();        // 耗时
                        response.getContentLength();  // header中的数据长度（Content-Length）
                        response.getReadedLength();   // 实际读取的数据长度

                        StatisticsInfo sta = liteHttp.getStatisticsInfo();
                        sta.getConnectTime();         // litehttp 实例化后所有请求耗时累计
                        sta.getDataLength();          // litehttp 实例化后读取数据长度累计
                    }

                    @Override
                    public void onFailure(HttpException e, Response<File> response) {
                        mainHandler.postTask(() ->HttpUtil.showTips(getContext(), "Traffic/Time Statistics", e.toString()));
                    }
                }));
                break;
            case 13:
                // 13. Retry/Redirect
                // default retry times
                liteHttp.getConfig().setDefaultMaxRetryTimes(3);
                // default redirect times
                liteHttp.getConfig().setDefaultMaxRedirectTimes(4);
                // default retry waitting time

                // make request
                StringRequest redirect = new StringRequest(redirectUrl)
                    .setMaxRetryTimes(2) // maximum retry times
                    .setMaxRedirectTimes(5) // maximum redirect times
                    .setHttpListener(new HttpListener<String>() {

                        @Override
                        public void onRedirect(AbstractRequest<String> request, int max, int times) {
                            mainHandler.postTask(() ->
                            (new ToastDialog(getContext())).setText("Redirect max num: " + max + " , times: " + times
                                    + "\n GO-TO: ").show());
                        }

                        @Override
                        public void onRetry(AbstractRequest<String> request, int max, int times) {
                            mainHandler.postTask(() ->
                            (new ToastDialog(getContext())).setText("Retry Now! max num: " + max + " , times: " + times).show());
                        }

                        @Override
                        public void onSuccess(String s, Response<String> response) {
                            mainHandler.postTask(() ->
                            HttpUtil.showTips(getContext(), "Retry/Redirect", "Content Length: " + s.length()));
                        }
                    });

                liteHttp.executeAsync(redirect);
                break;

            case 14:
                Ability ability = new MainAbility();
                // 14. Best Practices of Exception Handling
                class MyHttpListener<T> extends HttpListener<T> {
                    private Ability ability;

                    public MyHttpListener(Ability ability) {
                        this.ability = ability;
                    }

                    // disable listener when activity is null or be finished.
                    @Override
                    public boolean disableListener() {
                        return ability == null || ability.isTerminating();
                    }

                    // handle by this by call super.onFailure()
                    @Override
                    public void onFailure(HttpException e, Response response) {
                        // handle exception
                        new MyHttpExceptHandler(ability).handleException(e);
                    }
                }

                liteHttp.executeAsync(new StringRequest("http://baidu.com").setHttpListener(
                    new MyHttpListener<String>(ability) {
                        @Override
                        public void onSuccess(String s, Response<String> response) {
                            super.onSuccess(s, response);
                        }

                        @Override
                        public void onFailure(HttpException e, Response response) {
                            // handle by this by call super.onFailure()
                            super.onFailure(e, response);
                            // 通过调用父类的处理方法，来调用 MyHttpExceptHandler 来处理异常。
                        }
                    }));
                break;
            case 15:
                // 15. Best Practices of Cancel Request
                StringRequest stringRequest = new StringRequest(redirectUrl).setHttpListener(
                        new HttpListener<String>() {
                            @Override
                            public void onCancel(String s, Response<String> response) {
                                mainHandler.postTask(() ->
                                HttpUtil.showTips(
                                        getContext(), "Cancel Request",
                                        "Request Canceld: " + response.getRequest().isCancelled()));
                            }
                        });
                liteHttp.executeAsync(stringRequest);
                Time.sleep(100);
                stringRequest.cancel();
                break;
            case 16:
                // 16. POST Multi-Form Data
//                LayoutScatter scatter1 = LayoutScatter.getInstance(this);
//                ComponentContainer processBar1 = (ComponentContainer) scatter1.parse(ResourceTable.Layout_processBar_dialog, null, true);
//
//                CommonDialog commonDialog1 = new CommonDialog(this);
//                //commonDialog.setTransparent(true);
//                commonDialog1.setContentCustomComponent(processBar1);
//                commonDialog1.setTitleText("File Upload");

                ProgressBar postProgress = (ProgressBar) findComponentById(ResourceTable.Id_progressbar);
                String uploadUrl1 = "/multipart";
                final StringRequest postRequest = new StringRequest(uploadUrl1)
                    .setMethod(HttpMethods.Post)
                    .setHttpListener(new HttpListener<String>(true, false, true) {
                        @Override
                        public void onStart(AbstractRequest<String> request) {
                            super.onStart(request);
                            //postProgress.show();
                        }

                        @Override
                        public void onUploading(AbstractRequest<String> request, long total, long len) {
                            mainHandler.postTask(() ->
                            {
                                postProgress.setMaxValue((int) total);
                                postProgress.setProgressValue((int) len);
                            });
                        }

                        @Override
                        public void onEnd(Response<String> response) {
                            if (postProgress != null) {
                                mainHandler.postTask(() ->
                                postProgress.release());
                            }
                            if (response.isConnectSuccess()) {
                                mainHandler.postTask(() ->
                                HttpUtil.showTips(getContext(), "Upload Success", response.getResult() + ""));
                            } else {
                                mainHandler.postTask(() ->
                                HttpUtil.showTips(getContext(), "Upload Failure", response.getException() + ""));
                            }
                            response.printInfo();
                        }
                    });
                mainHandler.postTask(() ->
                {
                    listDialog = new ListDialog(this, ListDialog.NORMAL);
                    listDialog.setSize(600, 1100);
                    listDialog.setItems(new String[]{
                            "字符串上传",
                            "UrlEncodedForm上传",
                            "对象自动转JSON上传",
                            "对象序列化后上传",
                            "字节上传",
                            "单文件上传",
                            "单输入流上传",
                            "多文件（表单）上传"});
                    listDialog.setTitleText("  POST DATA TEST");
                    listDialog.setButton(1, "确定", new IDialog.ClickedListener() {
                        @Override
                        public void onClick(IDialog iDialog, int i) {
                            listDialog.destroy();
                        }
                    });
                    listDialog.show();
                    listDialog.getListContainer().setPadding(50, 10, 10, 0);
                    listDialog.setOnSingleSelectListener(new IDialog.ClickedListener() {
                        FileInputStream fis = null;

                        @Override
                        public void onClick(IDialog iDialog, int i) {
                            switch (i) {
                                case 0:
                                    postRequest.setHttpBody(new StringBody("hello lite: 你好，Lite！"));
                                    break;
                                case 1:
                                    LinkedList<NameValuePair> pList = new LinkedList<NameValuePair>();
                                    pList.add(new NameValuePair("key1", "value-haha"));
                                    pList.add(new NameValuePair("key2", "value-hehe"));
                                    postRequest.setHttpBody(new UrlEncodedFormBody(pList));
                                    break;
                                case 2:
                                    postRequest.setHttpBody(new JsonBody(new UserParam(168, "haha-key")));
                                    break;
                                case 3:
                                    ArrayList<String> list = new ArrayList<String>();
                                    list.add("a");
                                    list.add("b");
                                    list.add("c");
                                    postRequest.setHttpBody(new SerializableBody(list));
                                    break;
                                case 4:
                                    postRequest.setHttpBody(new ByteArrayBody(new byte[]{1, 2, 3, 4, 5, 15, 18, 127}));
                                    break;
                                case 5:
                                    postRequest.setHttpBody(new FileBody(new File(absolutePath + "/1.jpg")));
                                    break;
                                case 6:
                                    try {
                                        fis = new FileInputStream(new File("/sdcard/user.txt"));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    postRequest.setHttpBody(new InputStreamBody(fis));
                                    break;
                                case 7:

                                    String absolutePath2 = getContext().getExternalCacheDir().getPath();
                                    fis = null;
//                        File file = new File(absolutePath2);
                                    try {
                                        fis = new FileInputStream(new File(absolutePath2 + "/2.jpg"));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    MultipartBody body = new MultipartBody();
                                    body.addPart(new StringPart("key1", "hello"));
                                    body.addPart(new StringPart("key2", "很高兴见到你", "utf-8", null));
                                    body.addPart(new BytesPart("key3", new byte[]{1, 2, 3}));
                                    body.addPart(new FilePart("pic", new File(absolutePath2 + "/1.jpg"), "image/jpeg"));
                                    body.addPart(new InputStreamPart("litehttp", fis, "user.txt", "text/plain"));
                                    postRequest.setHttpBody(body);
                                    break;
                            }
                            liteHttp.executeAsync(postRequest);
                        }
                    });
                });


                break;
            case 17:
                // 17. Concurrent and Scheduling
                HttpConfig httpConfig = liteHttp.getConfig();
                // only one task can be executed at the same time
                httpConfig.setConcurrentSize(1);
                // at most two tasks be hold in waiting queue at the same time
                httpConfig.setWaitingQueueSize(2);
                // the last waiting task executed first
                httpConfig.setSchedulePolicy(SchedulePolicy.LastInFirstRun);
                // when task more than 3(current = 1, waiting = 2), new task will be discard.
                httpConfig.setOverloadPolicy(OverloadPolicy.DiscardCurrentTask);

                // note : restore config to default, next click.
                needRestore = true;

                // by [DiscardCurrentTask Policy] the last will be discard.
                for (int i = 0; i < 4; i++) {
                    liteHttp.executeAsync(new StringRequest(url).setTag(i));
                }
                // submit order : 0 -> 1 -> 2 -> 3
                // task 0 is executing, 1 and 2 is in waitting queue, 3 was discarded.
                // real executed order: 0 -> 2 -> 1

                break;
            case 18:
                // 18. Detail of Configuration

                // init common headers for all request
                List<NameValuePair> headers = new ArrayList<NameValuePair>();
                headers.add(new NameValuePair("cookies", "this is cookies"));
                headers.add(new NameValuePair("custom-key", "custom-value"));

                HttpConfig newConfig = liteHttp.getConfig();
                // app context(be used to detect network and get app files path)
                newConfig.setContext(getContext());
                // the log is turn on when debugged is true
                newConfig.setDebugged(true);
                // set user-agent
                newConfig.setUserAgent("Mozilla/5.0");
                // set global http listener to all request
                newConfig.setGlobalHttpListener(null);
                // set global scheme and host to all request.
//                newConfig.setBaseUrl("http://litesuits.com");
                newConfig.setBaseUrl("https://getman.cn");
                // common headers will be set to all request
                newConfig.setCommonHeaders(headers);
                // set default cache path to all request
                newConfig.setDefaultCacheDir(Environment.DIRECTORY_DOWNLOADS + "/a-cache");
                // set default cache expire time to all request
                newConfig.setDefaultCacheExpireMillis(30 * 60 * 1000);
                // set default cache mode to all request
                newConfig.setDefaultCacheMode(CacheMode.NetFirst);
                // set default charset to all request
                newConfig.setDefaultCharSet("utf-8");
                // set default http method to all request
                newConfig.setDefaultHttpMethod(HttpMethods.Get);
                // set default maximum redirect-times to all request
                newConfig.setDefaultMaxRedirectTimes(5);
                // set default maximum retry-times to all request
                newConfig.setDefaultMaxRetryTimes(1);
                // set defsult model query builder to all request
                newConfig.setDefaultModelQueryBuilder(new JsonQueryBuilder());
                // whether to detect network before conneting.
                newConfig.setDetectNetwork(true);
                // disable some network
                newConfig.setDisableNetworkFlags(HttpConfig.FLAG_NET_DISABLE_NONE);
                // whether open the traffic & time statistics
                newConfig.setDoStatistics(true);
                // set connect timeout: 10s
                newConfig.setConnectTimeout(10000);
                // set socket timeout: 10s
                newConfig.setSocketTimeout(10000);
                // if the network is unstable, wait 3000 milliseconds then start retry.
                newConfig.setRetrySleepMillis(3000);
                // set maximum size of memory cache space
                newConfig.setMaxMemCacheBytesSize(1024 * 300);
                // maximum number of concurrent tasks(http-request) at the same time
                newConfig.setConcurrentSize(3);
                // maximum number of waiting tasks(http-request) at the same time
                newConfig.setWaitingQueueSize(100);
                // set overload policy of thread pool executor
                newConfig.setOverloadPolicy(OverloadPolicy.DiscardOldTaskInQueue);
                // set schedule policy of thread pool executor
                newConfig.setSchedulePolicy(SchedulePolicy.LastInFirstRun);
                break;
            case 19:
                // 19. Usage of Annotation
                @HttpUri(userUrl)
                @HttpMethod(HttpMethods.Get)
                @HttpID(1)
                @HttpCacheMode(CacheMode.CacheFirst)
                @HttpCacheExpire(value = 1, unit = TimeUnit.MINUTES)
                class UserAnnoParam implements HttpParamModel {
                    public long id = 110;
                    private String key = "aes";
                }

                liteHttp.executeAsync(new JsonRequest<User>(
                        new UserAnnoParam(), User.class) {}.setHttpListener(
                        new HttpListener<User>() {
                            @Override
                            public void onSuccess(User user, Response<User> response) {
                                mainHandler.postTask(() ->
                                HttpUtil.showTips(getContext(), "Annotation Param", user.toString()));
                            }
                        }));
                break;
            case 20:
                // 20. Multi Cache Mechanism
                StringRequest cacheRequest = new StringRequest(url);

                cacheRequest.setCacheMode(CacheMode.CacheFirst);
                cacheRequest.setCacheExpire(30, TimeUnit.SECONDS);
                cacheRequest.setCacheDir(absolutePath);
                cacheRequest.setCacheKey(null);

                cacheRequest.setHttpListener(new HttpListener<String>() {
                    @Override
                    public void onSuccess(String html, Response<String> response) {
                        String title = response.isCacheHit() ? "Hit Cache(使用缓存)" : "No Cache(未用缓存)";
                        mainHandler.postTask(() ->
                        HttpUtil.showTips(getContext(), title, html));
                    }
                });
                liteHttp.executeAsync(cacheRequest);
                break;
            case 21:
                // 21. CallBack Mechanism
                // the correct way to set global http listener for all request.
                liteHttp.getConfig().setGlobalHttpListener(globalHttpListener);
                /**
                 * new http listener for current request:
                 *
                 * runOnUiThread = false;
                 * readingNotify = false;
                 * uploadingNotify = false;
                 *
                 * actually you can set a series of http listener for one http request.
                 */
                HttpListener<PixelMap> firstHttpListener = new HttpListener<PixelMap>(false, false, false) {
                    @Override
                    public void onSuccess(PixelMap bitmap, Response<PixelMap> response) {
                        //HttpLog.i(TAG, "first Listener, request success ...");
                        HiLog.debug(new HiLogLabel(HiLog.DEBUG, 0x00101, TAG),"first Listener, request success ...");
                    }

                    @Override
                    public void onFailure(HttpException e, Response<PixelMap> response) {
                        //HttpLog.i(TAG, "first Listener, request failure ...");
                        HiLog.debug(new HiLogLabel(HiLog.DEBUG, 0x00101, TAG),"first Listener, request failure ...");
                    }

                    @Override
                    public void onLoading(AbstractRequest<PixelMap> request, long total, long len) {
                        //HttpLog.i(TAG, "first Listener, request loading  ...");
                        HiLog.debug(new HiLogLabel(HiLog.DEBUG, 0x00101, TAG),"first Listener, request loading  ...");
                    }
                };
                // create a bitmap request.
                BitmapRequest bitmapRequest = new BitmapRequest(picUrl);

                // correct way to set first http listener
                bitmapRequest.setHttpListener(firstHttpListener);
                // correct way to set secondary (linked)listener
                firstHttpListener.setLinkedListener(secondaryListener);

                //load and show bitmap
                liteHttp.executeAsync(bitmapRequest);

                break;
            case 22:
                // 22. Best Practices of SmartExecutor
                //可定义等待队列进入执行状态的策略：先来先执行，后来先执行。

                //可定义等待队列满载后处理新请求的策略：
                //- 抛弃队列中最新的任务
                //- 抛弃队列中最旧的任务
                //- 抛弃当前新任务
                //- 直接执行（阻塞当前线程）
                //- 抛出异常（中断当前线程）

                // 智能并发调度控制器：设置[最大并发数]，和[等待队列]大小
                SmartExecutor smallExecutor = new SmartExecutor();

                // set this temporary parameter, just for test

                // number of concurrent threads at the same time, recommended core size is CPU count
                smallExecutor.setCoreSize(2);

                // adjust maximum number of waiting queue size by yourself or based on phone performance
                smallExecutor.setQueueSize(2);

                // 任务数量超出[最大并发数]后，自动进入[等待队列]，等待当前执行任务完成后按策略进入执行状态：后进先执行。
                smallExecutor.setSchedulePolicy(SchedulePolicy.LastInFirstRun);

                // 后续添加新任务数量超出[等待队列]大小时，执行过载策略：抛弃队列内最旧任务。
                smallExecutor.setOverloadPolicy(OverloadPolicy.DiscardOldTaskInQueue);

                // 一次投入 4 个任务
                for (int i = 0; i < 4; i++) {
                    final int j = i;
                    smallExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            HttpLog.i(TAG, " TASK " + j + " is running now ----------->");
                            Time.sleep(j * 200);
                        }
                    });
                }

                // 再投入1个需要取消的任务
                Future future = smallExecutor.submit(new Runnable() {
                    @Override
                    public void run() {
                        HttpLog.i(TAG, " TASK 4 will be canceled ... ------------>");
                        Time.sleep(1000);
                    }
                });
                future.cancel(false);
                break;
            case 23:
                // 23. Automatic Conversion of Complex Model
                // 模拟用户登录
                liteHttp.executeAsync(new LoginParam("123", "456").setHttpListener(
                        new HttpListener<User>() {
                            @Override
                            public void onSuccess(User user, Response<User> response) {
                                mainHandler.postTask(() ->
                                HttpUtil.showTips(getContext(), "S:Json Convertor", user.toString()));
                            }

                            @Override
                            public void onFailure(HttpException e, Response<User> response) {
                                mainHandler.postTask(() ->
                                HttpUtil.showTips(getContext(), "F:Json Convertor", e.getMessage()));
                            }
                        }
                ));
                break;
            case 24:
                // 24. Best Practice: HTTP Rich Param Model (It is simpler and More Useful)

                // rich param 更简单、有用！只需要定义一个RichParam，可指定URL、参数、返回响应体三个关键事物。

                // request : http://litesuits.com/mockdata/user_get?id=110&key=aes-125
                // response: User
                @HttpUri("{url}/{path}")
                class UserRichParam extends HttpRichParamModel<User> {

                    @NonHttpParam
                    @HttpReplace("url")
//                    private String url = "https://getman.cn";
                    private String url = "http://192.168.1.156:8080";

                    @NonHttpParam
                    @HttpReplace("path")
//                    private String path = "/mockdata/user_get";
                    private String path = "user_get";

                    public long id = 110;
                    private String key = "aes-125";
                }

                // 一句话调用即可
                JsonRequest<User> userJsonRequest = liteHttp.executeAsync(new UserRichParam());


                // 其他更多注解还有：
                @HttpBaseUrl("{host}") // 定义scheme，使用host变量的值取代
                @HttpUri("{path}") // 定义uri 或者 path
                @HttpMethod(HttpMethods.Get) // 请求方式
                @HttpCharSet("UTF-8") // 请求编码
                @HttpTag("custom tag") // 打TAG
                @HttpCacheMode(CacheMode.CacheFirst) // 缓存模式
                @HttpCacheKey("custom-cache-key-name-by-myself") // 缓存文件名字
                @HttpCacheExpire(value = 1, unit = TimeUnit.MINUTES) // 缓存时间
                @HttpID(2) // 请求ID
                @HttpMaxRetry(3) // 重试次数
                @HttpMaxRedirect(5)
                        // 重定向次数
                class TEST extends HttpRichParamModel<User> {

                    @NonHttpParam
                    @HttpReplace("host")
//                    private String host = "http://litesuits.com";
                    private String host = "http://192.168.1.156:8080";

                    @NonHttpParam
                    @HttpReplace("path")
//                    private String apiPath = "/mockdata/user_get";
                    private String path = "/user_get";


                    // 可以复写设置headers/attachToUrl/listener/httpbody 等参数
                    /**
                     * 返回true则将将成员变量{@link #host}、{@link #path}拼接到url中
                     * 返回false，则不拼接。
                     * @return
                     */
                    @Override
                    public boolean isFieldsAttachToUrl() {
                        return false;
                    }

                    @Override
                    protected LinkedHashMap<String, String> createHeaders() {
                        return super.createHeaders();
                    }

                    @Override
                    protected HttpListener<User> createHttpListener() {
                        return super.createHttpListener();
                    }

                    @Override
                    protected HttpBody createHttpBody() {
                        return super.createHttpBody();
                    }
                }
                liteHttp.executeAsync(new TEST().setHttpListener(new HttpListener<User>() {
                    @Override
                    public void onSuccess(User user, Response<User> response) {
                        mainHandler.postTask(() ->
                        HttpUtil.showTips(getContext(), "Rich Param Model", user.toString()));
                    }

                    @Override
                    public void onFailure(HttpException e, Response<User> response) {
                        mainHandler.postTask(() ->
                        HttpUtil.showTips(getContext(), "Rich Param Failure", e.getMessage()));
                    }
                }));
                break;
        }
    }

    @Override
    protected void initData() {
    }

    private void initListener() {

        tcIndicator[0] = tcIndicator0;
        tcIndicator[1] = tcIndicator1;
        tcIndicator[2] = tcIndicator2;
        tcIndicator[3] = tcIndicator3;
        tcIndicator[4] = tcIndicator4;
        tcIndicator[5] = tcIndicator5;
        tcIndicator[6] = tcIndicator6;
        tcIndicator[7] = tcIndicator7;
        tcIndicator[8] = tcIndicator8;
        tcIndicator[9] = tcIndicator9;
        tcIndicator[10] = tcIndicator10;
        tcIndicator[11] = tcIndicator11;
        tcIndicator[12] = tcIndicator12;
        tcIndicator[13] = tcIndicator13;
        tcIndicator[14] = tcIndicator14;
        tcIndicator[15] = tcIndicator15;
        tcIndicator[16] = tcIndicator16;
        tcIndicator[17] = tcIndicator17;
        tcIndicator[18] = tcIndicator18;
        tcIndicator[19] = tcIndicator19;
        tcIndicator[20] = tcIndicator20;
        tcIndicator[21] = tcIndicator21;
        tcIndicator[22] = tcIndicator22;
        tcIndicator[23] = tcIndicator23;
        tcIndicator[24] = tcIndicator24;
        for (int i = 0; i < tcIndicator.length; i++) {
            tcIndicator[i].setClickedListener(this);
        }
    }

    private void initList() {
    }

    @Override
    public void onClick(Component component) {
        for (int i = 0; i < tcIndicator.length; i++) {
            if (tcIndicator[i].getId() == component.getId()) {
                categoryId = i;
                break;
            }
        }
        initLiteHttp();
        clickTestItem(categoryId);
        setClickColor(categoryId);
    }

    private void setClickColor(int position) {
        for (int i = 0; i < tcIndicator.length; i++) {
            tcIndicator[i].setTextColor(new Color(COLOR_RED));
        }
        tcIndicator[position].setTextColor(new Color(COLOR_BLACK));
    }

    /**
     * 单例 keep an singleton instance of litehttp
     */
    private void initLiteHttp() {
        if (liteHttp == null) {
            liteHttp = LiteHttp.build(this)
                    .setHttpClient(new HttpUrlClient())       // http client
                    .setJsonConvertor(new GsonImpl())        // json convertor
                    .setBaseUrl(baseUrl)                    // set base url
                    .setDebugged(true)                     // log output when debugged
                    .setDoStatistics(true)                // statistics of time and traffic
                    .setDetectNetwork(true)              // detect network before connect
                    .setUserAgent("Mozilla/5.0 (...)")  // set custom User-Agent
                    .setSocketTimeout(10000)           // socket timeout: 10s
                    .setConnectTimeout(10000)         // connect timeout: 10s
                    .create();
        } else {

            liteHttp.getConfig()                   // configuration directly
                    .setSocketTimeout(5000)       // socket timeout: 5s
                    .setConnectTimeout(5000);    // connect timeout: 5s
            HttpUrlClient url = new HttpUrlClient();
            //url.setHostnameVerifier()
        }
    }
    // 实现登陆，参数为 name 和 password，成功后返回 User 对象。
    @HttpUri(loginUrl)
    @HttpMethod(HttpMethods.Post)
    class LoginParam extends HttpRichParamModel<User> {
        private String name;
        private String password;

        public LoginParam(String name, String password) {
            this.name = name;
            this.password = password;
        }
    }
    // 实现登陆，参数为 name 和 password，成功后返回 User 对象。
    @HttpUri(uploadUrl)
    @HttpMethod(HttpMethods.Post)
    class UploadParam extends HttpRichParamModel {
        private LinkedHashMap<String, String> headers;
        private String filename;

        public UploadParam(LinkedHashMap<String, String> headers, String filename) {
            this.headers = headers;
            this.filename = filename;
        }

        @Override
        protected LinkedHashMap<String, String> createHeaders() {
            return headers;
        }


        @Override
        protected HttpBody createHttpBody() { return super.createHttpBody(); }
    }
    /**
     * global http listener for all request.
     */
    GlobalHttpListener globalHttpListener = new GlobalHttpListener() {
        @Override
        public void onStart(AbstractRequest<?> request) {
            HttpLog.i(TAG, "Global, request start ...");
        }

        @Override
        public void onSuccess(Object data, Response<?> response) {
            HttpLog.i(TAG, "Global, request success ..." + data);
        }

        @Override
        public void onFailure(HttpException e, Response<?> response) {
            HttpLog.i(TAG, "Global, request failure ..." + e);
        }

        @Override
        public void onCancel(Object data, Response<?> response) {
            HttpLog.i(TAG, "Global, request cancel ..." + data);
        }
    };

    /**
     * http listener for current reuqest:
     *
     * runOnUiThread = true;
     * readingNotify = true;
     * uploadingNotify = true;
     */
    HttpListener<PixelMap> secondaryListener = new HttpListener<PixelMap>(true, true, true) {
//        LayoutScatter scatter1 = LayoutScatter.getInstance(MainAbilitySlice.this);
//        ComponentContainer processBar1 = (ComponentContainer) scatter1.parse(ResourceTable.Layout_processBar_dialog, null, true);

//        CommonDialog commonDialog1 = new CommonDialog(MainAbilitySlice.this);

        ProgressBar progressDialog = null;

        @Override
        public void onStart(AbstractRequest<PixelMap> request) {
            HttpLog.i(TAG, "second listener, request start ...");
            mainHandler.postTask(() ->
            {
                progressDialog = new ProgressBar(MainAbilitySlice.this);
                commonDialog.setContentCustomComponent(processBar);
                commonDialog.setTitleText("Second listener");
                commonDialog.setDuration(2000);
                commonDialog.show();
            });
        }

        @Override
        public void onSuccess(PixelMap bitmap, Response<PixelMap> response) {
            HttpLog.i(TAG, "second listener, request success ...");
            mainHandler.postTask(() ->
            {
                commonDialog.remove();

                Image image = new Image(getContext());
                image.setHeight(500);
                image.setWidth(300);
                image.setMarginTop(20);
                image.setMarginLeft(20);
                image.setPixelMap(bitmap);
                image.setScaleMode(Image.ScaleMode.ZOOM_CENTER);

                commonDialog.setContentCustomComponent(image);
                commonDialog.show();
            });
        }

        @Override
        public void onFailure(HttpException e, Response<PixelMap> response) {
            HttpLog.i(TAG, " second listener, request failure ...");
            mainHandler.postTask(() ->
            commonDialog.remove());
        }

        @Override
        public void onLoading(AbstractRequest<PixelMap> request, long total, long len) {
            HttpLog.i(TAG, " second listener, request loading  ...");
            mainHandler.postTask(() ->
            {
                progressDialog.setMaxValue((int) total);
                progressDialog.setProgressValue((int) len);
            });
        }

    };
}
