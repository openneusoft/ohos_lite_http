package com.litesuits.http_sample.model;

import java.io.Serializable;

/**
 * base model
 */
public abstract class BaseModel implements Serializable {}