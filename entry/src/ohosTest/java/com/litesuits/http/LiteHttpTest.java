package com.litesuits.http;
import com.litesuits.go.SmartExecutor;
import com.litesuits.http.request.content.*;
import com.litesuits.http.request.content.multi.*;
import com.litesuits.http.request.param.*;
import com.alibaba.fastjson.JSONObject;
import com.litesuits.go.OverloadPolicy;
import com.litesuits.go.SchedulePolicy;
import com.litesuits.http.annotation.*;
import com.litesuits.http.data.*;
import com.litesuits.http.exception.HttpException;
import com.litesuits.http.impl.huc.HttpUrlClient;
import com.litesuits.http.listener.GlobalHttpListener;
import com.litesuits.http.listener.HttpListener;
import com.litesuits.http.log.HttpLog;
import com.litesuits.http.request.*;
import com.litesuits.http.request.query.JsonQueryBuilder;
import com.litesuits.http.response.Response;
import com.litesuits.http.utils.HttpUtil;
import com.litesuits.http_sample.MainAbility;
import com.litesuits.http_sample.custom.CustomJSONParser;
import com.litesuits.http_sample.custom.MyHttpExceptHandler;
import com.litesuits.http_sample.model.User;
import com.litesuits.http_sample.model.UserParam;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.ProgressBar;
import ohos.app.Context;
import ohos.app.Environment;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.media.image.PixelMap;
import ohos.miscservices.timeutility.Time;
import org.junit.Assert;
import org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class LiteHttpTest {

    private boolean needRestore;
    protected static LiteHttp liteHttp;
    public static String url = "https://www.baidu.com";
    public static String baseUrl = "https://getman.cn";
    public static final String userUrl = "/echo";
    public static String httpsUrl = "https://www.baidu.com";
    public static final String loginUrl = "https://getman.cn/echo/user_get";
    public Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    public static final String picUrl = "/u=1725422096,2181202425&fm=26&fmt=auto&gp=0.jpg";
    public static final String redirectUrl = "https://www.baidu.com/link?url=Lqc3GptP8u05JCRDsk0jqsAvIZh9WdtO_RkXYMYRQEm";
    public EventHandler mainHandler = null;
    int statuscode = 0;
    public String absolutePath;

    @Before
    public void before(){
        absolutePath = context.getExternalCacheDir().getPath();
        initLiteHttp(baseUrl);
        Assert.assertNotNull(liteHttp);
    }
    @Test
    public void case0() {
        liteHttp.getConfig()                   // configuration directly
                .setSocketTimeout(1000)       // socket timeout: 5s
                .setConnectTimeout(1000);    // connect timeout: 5s

        String apiUrl = "http://api.daily.taobao.net/router/rest?app_key=4272&format=json&method=cainiao.guoguo.courier.getrewardtip&open_id=4398046618004&session_code=7949690a30a91305a3a351a314ec501e&sign=0A9DDED897FDB04E1D8177C984AD4238&sign_method=md5&timestamp=2016-08-05&user_id=4398046618004&v=2.0";
        //https
        //String apiUrl = baseUrl + userUrl + "?id=1&key=esc";
        StringRequest login = new StringRequest(apiUrl)
            .setMethod(HttpMethods.Get)
            .setHttpListener(new HttpListener<String>() {
                @Override
                public void onSuccess(String s, Response<String> response) {
                    HttpStatus httpStatus = response.getHttpStatus();
                    statuscode = httpStatus.getCode();
                    Assert.assertEquals(statuscode,200);
                }
                @Override
                public void onFailure(HttpException e, Response<String> response) {
                    HttpStatus httpStatus = response.getHttpStatus();
                    if(httpStatus != null){
                        statuscode = httpStatus.getCode();
                    }else{
                        statuscode = -1;
                    }
                    Assert.assertNotEquals(statuscode,200);
                }
            });
        liteHttp.executeAsync(login);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case1() {
        final StringRequest request = new StringRequest("http://www.baidu111111.com/").setHttpListener(
            new HttpListener<String>() {
                @Override
                public void onSuccess(String s, Response<String> response) {
                    HttpStatus httpStatus = response.getHttpStatus();
                    statuscode = httpStatus.getCode();
                    Assert.assertEquals(statuscode,200);
                }
                @Override
                public void onFailure(HttpException e, Response<String> response) {
                    HttpStatus httpStatus = response.getHttpStatus();
                    if(httpStatus != null){
                        statuscode = httpStatus.getCode();
                    }else{
                        statuscode = -1;
                    }
                    Assert.assertNotEquals(statuscode,200);
                }
            }
        );
        liteHttp.executeAsync(request);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void case2_1() {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                // 2.0 execute: return fully response
                Response<User> response = liteHttp.execute(new LoginParam("a", "a"));
                User user = response.getResult();
                Assert.assertNotNull(user);
                if(user != null){
                    Assert.assertEquals(user.getData().getAge(), 18);
                    Assert.assertEquals(user.getData().getName(), "呱呱");
                }
            }
        };
        new Thread(run).start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case2_2(){
        Runnable run = new Runnable() {
            @Override
            public void run() {
                // 2.1 perform: return java model directly
                User user = liteHttp.perform(new JsonAbsRequest<User>(userUrl) {});
                Assert.assertNotNull(user);
                if(user != null){
                    Assert.assertEquals(user.getData().getAge(), 18);
                    Assert.assertEquals(user.getData().getName(), "呱呱");
                }
            }
        };
        new Thread(run).start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case2_3(){
        String baseUrl = "https://img2.baidu.com/it";
        initLiteHttp(baseUrl);
        Runnable run = new Runnable() {
            @Override
            public void run() {
                // 2.2 return data directly, handle result on current thread
                PixelMap bitmap = liteHttp.perform(new BitmapRequest(picUrl).setHttpListener(
                    new HttpListener<PixelMap>(false, true, true) {
                        @Override
                        public void onSuccess(PixelMap pixelMap, Response<PixelMap> response) {
                            Assert.assertNotNull(pixelMap);
                        }
                        @Override
                        public void onFailure(HttpException e, Response<PixelMap> response) {
                            Assert.assertNull(e);
                        }
                        @Override
                        public void onLoading(AbstractRequest<PixelMap> request, long total, long len) {}
                    }));
                if (bitmap != null) {
                    bitmap.release();
                }
            }
        };
        new Thread(run).start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case2_4(){
        Runnable run = new Runnable() {
            @Override
            public void run() {
                // 2.3 handle result on UI thread(主线程处理，注意HttpListener默认是在主线程回调)
                liteHttp.execute(new StringRequest(url).setHttpListener(
                    new HttpListener<String>(true) {
                        @Override
                        public void onSuccess(String data, Response<String> response) {
                            Assert.assertNotNull(data);
                        }

                        @Override
                        public void onFailure(HttpException e, Response<String> response) {
                            Assert.assertNull(e);
                        }
                    }
                ));
            }
        };
        new Thread(run).start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case3(){
        Runnable run = new Runnable() {
            @Override
            public void run() {
                // 3.0 simple get
                String result = liteHttp.get(url);
                Assert.assertNotNull(result);
                // 3.1 simple post
                result = liteHttp.post(new StringRequest(httpsUrl));
                Assert.assertNotNull(result);
                // 3.2 simple post and publish
                User user = liteHttp.get(userUrl, User.class);
                Assert.assertNotNull(user);
                // 3.3 simple head and return
                ArrayList<NameValuePair> headers = liteHttp.head(new StringRequest(url));
                Assert.assertNotNull(headers);
            }
        };
        Thread th = new Thread(run);
        th.setDaemon(true);
        th.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case4_1(){
        // http scheme error
        try {
            Response response = liteHttp.executeOrThrow(new BytesRequest("abc://eee.com"));
        } catch (HttpException e) {
            Assert.assertNotNull(e);
        }
    }
    @Test
    public void case4_2(){
        // java model translate error
        try {
            User user = liteHttp.performOrThrow(new JsonAbsRequest<User>("http://thanku.love") {});
        } catch (final Exception e) {
            if (mainHandler == null) {
                mainHandler = new EventHandler(EventRunner.getMainEventRunner());
            }
            mainHandler.postTask(new Runnable() {
                @Override
                public void run() {
                    Assert.assertNotNull(e);
                }
            });
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case5(){
        liteHttp.executeAsync(new StringRequest(httpsUrl).setHttpListener(
            new HttpListener<String>() {
                @Override
                public void onSuccess(String s, Response<String> response) {
                    Assert.assertNotNull(s);
                }

                @Override
                public void onFailure(HttpException e, Response<String> response) {
                    Assert.assertNull(e);
                }
            }
        ));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void case6_2(){
        /**
         * Request Model : json string translate to user object
         */
        class UserRequest extends JsonAbsRequest<User> {
            public UserRequest(String url, HttpParamModel param) {
                super(url, param);
            }
        }
        // build as http://{userUrl}?id=168&key=md5
        UserRequest userRequest = new UserRequest(userUrl, new UserParam(18, "md5"));
        userRequest.setHttpListener(new HttpListener<User>() {
            @Override
            public void onSuccess(User user, Response<User> response) {
                // data has been translated to user object
                Assert.assertNotNull(user);
            }

            @Override
            public void onFailure(HttpException e, Response<User> response) {
                Assert.assertNull(e);
            }
        });
        liteHttp.executeAsync(userRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case7(){
        JsonRequest<JSONObject> jsonRequest = new JsonRequest<JSONObject>(userUrl, JSONObject.class);
        jsonRequest.setHttpBody(new JsonBody(new UserParam(168, "haha-key")));
        jsonRequest.setDataParser(new CustomJSONParser());
        liteHttp.executeAsync(jsonRequest.setHttpListener(new HttpListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject jsonObject, Response<JSONObject> response) {
                Assert.assertNotNull(jsonObject);
            }

            @Override
            public void onFailure(HttpException e, Response<JSONObject> response) {
                Assert.assertNull(e);
            }
        }));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case8_1(){
        liteHttp.getConfig().setJsonConvertor(new FastJson());
//                String uj = "{\"api\":\"com.xx.get.userinfo\",\"v\":\"1.0\",\"code\":200,\"message\":\"success\",\"data\":{\"age\":18,\"name\":\"qingtianzhu\",\"girl_friends\":[\"xiaoli\",\"fengjie\",\"lucy\"]}}";
//                User u11 = Json.get().toObject(uj, User.class);

        // json model convert used #FastJson
        liteHttp.executeAsync(new JsonAbsRequest<User>(userUrl) {}
            .setHttpListener(new HttpListener<User>() {
                @Override
                public void onSuccess(User user, Response<User> response) {
                    response.printInfo();
                    Assert.assertNotNull(user);
                    needRestore = true;
                }

                @Override
                public void onFailure(HttpException e, Response<User> response) {
                    Assert.assertNull(e);
                }
            }));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case8_2(){
        // json model convert used #FastJson
        liteHttp.performAsync(new StringRequest(userUrl).setHttpListener(new HttpListener<String>() {
            @Override
            public void onSuccess(String s, Response<String> response) {
                User u = Json.get().toObject(s, User.class);
                Assert.assertNotNull(u);
                needRestore = true;
            }

            @Override
            public void onFailure(HttpException e, Response<String> response) {
                Assert.assertNull(e);
            }

            @Override
            public void onEnd(Response<String> response) {
                needRestore = true;
            }
        }));
        try {
            Thread.sleep(3500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case9_1(){
        this.initLiteHttp("https://img2.baidu.com/it");
        String picUrl1 = "/u=1725422096,2181202425&fm=26&fmt=auto&gp=0.jpg";
        // load and show bitmap
        liteHttp.executeAsync(
            new BitmapRequest(picUrl1).setHttpListener(new HttpListener<PixelMap>(true, true, false) {
                @Override
                public void onSuccess(PixelMap bitmap, Response<PixelMap> response) {
                    Assert.assertNotNull(bitmap);
                }

                @Override
                public void onFailure(HttpException e, Response<PixelMap> response) {
                    Assert.assertNull(e);
                }
            }));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case9_2(){
        this.initLiteHttp("https://img2.baidu.com/it");
        String picUrl1 = "/u=1725422096,2181202425&fm=26&fmt=auto&gp=0.jpg";
        // download a file to externalCacheDir.
        FileRequest fileRequest = new FileRequest(picUrl1, absolutePath + "/1.jpg");
        liteHttp.executeAsync(fileRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        File f = new File(absolutePath + "/1.jpg");
        Assert.assertNotEquals(f.length(), 0);
    }
    @Test
    public void case10(){
        String uploadUrl = "/upload";
        StringRequest uploadRequest = new StringRequest(uploadUrl);
        uploadRequest.setMethod(HttpMethods.Post)
            .setHttpBody(new FileBody(new File(absolutePath + "/1.jpg")))
            .setHttpListener(new HttpListener<String>(true, false, true) {
                @Override
                public void onSuccess(String s, Response<String> response) {
                    Assert.assertNotNull(s);
                }

                @Override
                public void onFailure(HttpException e, Response<String> response) {
                    Assert.assertNull(e);
                }
            });
        liteHttp.executeAsync(uploadRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case11(){
        // 11. Disable Some Network
        HttpConfig config = liteHttp.getConfig();
        // must set context
        config.setContext(context);
        // disable mobile(2G/3G/4G) and wifi network
        config.setDisableNetworkFlags(HttpConfig.FLAG_NET_DISABLE_MOBILE | HttpConfig.FLAG_NET_DISABLE_WIFI);
        url = "https://www.baidu.com";
        liteHttp.executeAsync(new StringRequest(url).setHttpListener(new HttpListener<String>() {
            @Override
            public void onSuccess(String s, Response<String> response) {
                Assert.assertNull(s);
            }
            @Override
            public void onFailure(HttpException e, Response<String> response) {
                Assert.assertEquals(e.toString(), "com.litesuits.http.exception.NetException: Current Network Is Disabled By Your Setting (已禁用该网络类型)");
            }
        }));
        needRestore = true;
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case12(){
        String baseUrl = "https://img2.baidu.com/it";
        this.initLiteHttp(baseUrl);
        // 12. Traffic/Time Statistics
        // turn on
        liteHttp.getConfig().setDoStatistics(true);
        // see detail   picUrl
        liteHttp.executeAsync(new FileRequest(url).setHttpListener(new HttpListener<File>() {
            long time = 0;
            long len = 0;
            @Override
            public void onSuccess(File file, Response<File> response) {
                String msg = "This request take time:" + response.getUseTime()
                        + ", readed length:" + response.getReadedLength()
                        + "  Global " + liteHttp.getStatisticsInfo();

                response.getRedirectTimes();  // 重定向的次数
                response.getRetryTimes();     // 重试的次数
                response.getUseTime();        // 耗时
                response.getContentLength();  // header中的数据长度（Content-Length）
                response.getReadedLength();   // 实际读取的数据长度
                time += response.getUseTime();
                len += response.getReadedLength();
                StatisticsInfo sta = liteHttp.getStatisticsInfo();
                sta.getConnectTime();         // litehttp 实例化后所有请求耗时累计
                sta.getDataLength();          // litehttp 实例化后读取数据长度累计
                Assert.assertEquals(len, sta.getDataLength());
                Assert.assertEquals(time, sta.getConnectTime());
            }

            @Override
            public void onFailure(HttpException e, Response<File> response) {
                Assert.assertNull(e);
            }
        }));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case13(){
        // 13. Retry/Redirect
        // default retry times
        liteHttp.getConfig().setDefaultMaxRetryTimes(3);
        // default redirect times
        liteHttp.getConfig().setDefaultMaxRedirectTimes(4);
        // default retry waitting time
        // make request
        StringRequest redirect = new StringRequest(redirectUrl)
            .setMaxRetryTimes(2) // maximum retry times
            .setMaxRedirectTimes(5) // maximum redirect times
            .setHttpListener(new HttpListener<String>() {

            @Override
            public void onRedirect(AbstractRequest<String> request, int max, int times) {
                Assert.assertEquals(max, 5);
            }

            @Override
            public void onRetry(AbstractRequest<String> request, int max, int times) {
                Assert.assertEquals(max, 2);
            }

            @Override
            public void onSuccess(String s, Response<String> response) {
                Assert.assertNotNull(s);
            }
        });
        liteHttp.executeAsync(redirect);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case14(){
        Ability ability = new MainAbility();
        // 14. Best Practices of Exception Handling
        class MyHttpListener<T> extends HttpListener<T> {
            private Ability ability;
            public MyHttpListener(Ability activity) {
                this.ability = activity;
            }
            // disable listener when activity is null or be finished.
            @Override
            public boolean disableListener() {
                return ability == null || ability.isTerminating();
            }
            // handle by this by call super.onFailure()
            @Override
            public void onFailure(HttpException e, Response response) {
                // handle exception
                new MyHttpExceptHandler(ability).handleException(e);
            }
        }
        liteHttp.executeAsync(new StringRequest("https://baidu.com").setHttpListener(
            new MyHttpListener<String>(ability) {
                @Override
                public void onSuccess(String s, Response<String> response) {
                    Assert.assertNotNull(s);
                    super.onSuccess(s, response);
                }

                @Override
                public void onFailure(HttpException e, Response response) {
                    Assert.assertNull(e);
                    // handle by this by call super.onFailure()
                    super.onFailure(e, response);
                    // 通过调用父类的处理方法，来调用 MyHttpExceptHandler 来处理异常。
                }
            }));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case15(){
        // 15. Best Practices of Cancel Request
        StringRequest stringRequest = new StringRequest(redirectUrl).setHttpListener(
            new HttpListener<String>() {
                @Override
                public void onCancel(String s, Response<String> response) {
                    Assert.assertTrue(response.getRequest().isCancelled());
                }
            });
        liteHttp.executeAsync(stringRequest);
        Time.sleep(100);
        stringRequest.cancel();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case16_0(){
        String uploadUrl1 = "/post";
        final StringRequest postRequest = new StringRequest(uploadUrl1)
            .setMethod(HttpMethods.Post)
            .setHttpListener(new HttpListener<String>(true, false, true) {
                @Override
                public void onStart(AbstractRequest<String> request) {
                    super.onStart(request);
                }
                @Override
                public void onUploading(AbstractRequest<String> request, long total, long len) {

                }
                @Override
                public void onEnd(Response<String> response) {
                    if (response.isConnectSuccess()) {
                        Assert.assertNotNull(response.getResult());
                    } else {
                        Assert.assertNull(response.getException());
                    }
                }
            });
        postRequest.setHttpBody(new StringBody("hello lite: 你好，Lite！"));
        liteHttp.executeAsync(postRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case16_1(){
        String uploadUrl1 = "/multipart";
        final StringRequest postRequest = new StringRequest(uploadUrl1)
            .setMethod(HttpMethods.Post)
            .setHttpListener(new HttpListener<String>(true, false, true) {
                @Override
                public void onStart(AbstractRequest<String> request) {
                    super.onStart(request);
                }
                @Override
                public void onUploading(AbstractRequest<String> request, long total, long len) {

                }
                @Override
                public void onEnd(Response<String> response) {
                    if (response.isConnectSuccess()) {
                        Assert.assertNotNull(response.getResult());
                    } else {
                        Assert.assertNull(response.getException());
                    }
                }
            });
        LinkedList<NameValuePair> pList = new LinkedList<NameValuePair>();
        pList.add(new NameValuePair("key1", "value-haha"));
        pList.add(new NameValuePair("key2", "value-hehe"));
        postRequest.setHttpBody(new UrlEncodedFormBody(pList));
        liteHttp.executeAsync(postRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case16_2(){
        String uploadUrl1 = "/multipart";
        final StringRequest postRequest = new StringRequest(uploadUrl1)
            .setMethod(HttpMethods.Post)
            .setHttpListener(new HttpListener<String>(true, false, true) {
                @Override
                public void onStart(AbstractRequest<String> request) {
                    super.onStart(request);
                }
                @Override
                public void onUploading(AbstractRequest<String> request, long total, long len) {

                }
                @Override
                public void onEnd(Response<String> response) {
                    if (response.isConnectSuccess()) {
                        Assert.assertNotNull(response.getResult());
                    } else {
                        Assert.assertNull(response.getException());
                    }
                }
            });
        postRequest.setHttpBody(new JsonBody(new UserParam(168, "haha-key")));
        liteHttp.executeAsync(postRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case16_3(){
        String uploadUrl1 = "/multipart";
        final StringRequest postRequest = new StringRequest(uploadUrl1)
            .setMethod(HttpMethods.Post)
            .setHttpListener(new HttpListener<String>(true, false, true) {
                @Override
                public void onStart(AbstractRequest<String> request) {
                    super.onStart(request);
                }
                @Override
                public void onUploading(AbstractRequest<String> request, long total, long len) {

                }
                @Override
                public void onEnd(Response<String> response) {
                    if (response.isConnectSuccess()) {
                        Assert.assertNotNull(response.getResult());
                    } else {
                        Assert.assertNull(response.getException());
                    }
                }
            });
        ArrayList<String> list = new ArrayList<String>();
        list.add("a");
        list.add("b");
        list.add("c");
        postRequest.setHttpBody(new SerializableBody(list));
        liteHttp.executeAsync(postRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case16_4(){
        String uploadUrl1 = "/multipart";
        final StringRequest postRequest = new StringRequest(uploadUrl1)
            .setMethod(HttpMethods.Post)
            .setHttpListener(new HttpListener<String>(true, false, true) {
                @Override
                public void onStart(AbstractRequest<String> request) {
                    super.onStart(request);
                }
                @Override
                public void onUploading(AbstractRequest<String> request, long total, long len) {

                }
                @Override
                public void onEnd(Response<String> response) {
                    if (response.isConnectSuccess()) {
                        Assert.assertNotNull(response.getResult());
                    } else {
                        Assert.assertNull(response.getException());
                    }
                }
            });
        postRequest.setHttpBody(new ByteArrayBody(new byte[]{1, 2, 3, 4, 5, 15, 18, 127}));
        liteHttp.executeAsync(postRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case16_5(){
        String uploadUrl1 = "/multipart";
        final StringRequest postRequest = new StringRequest(uploadUrl1)
            .setMethod(HttpMethods.Post)
            .setHttpListener(new HttpListener<String>(true, false, true) {
                @Override
                public void onStart(AbstractRequest<String> request) {
                    super.onStart(request);
                }
                @Override
                public void onUploading(AbstractRequest<String> request, long total, long len) {

                }
                @Override
                public void onEnd(Response<String> response) {
                    if (response.isConnectSuccess()) {
                        Assert.assertNotNull(response.getResult());
                    } else {
                        Assert.assertNull(response.getException());
                    }
                }
            });
        postRequest.setHttpBody(new FileBody(new File(absolutePath + "/1.jpg")));
        liteHttp.executeAsync(postRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case16_6(){
        String uploadUrl1 = "/multipart";
        final StringRequest postRequest = new StringRequest(uploadUrl1)
            .setMethod(HttpMethods.Post)
            .setHttpListener(new HttpListener<String>(true, false, true) {
                @Override
                public void onStart(AbstractRequest<String> request) {
                    super.onStart(request);
                }
                @Override
                public void onUploading(AbstractRequest<String> request, long total, long len) {

                }
                @Override
                public void onEnd(Response<String> response) {
                    if (response.isConnectSuccess()) {
                        Assert.assertNotNull(response.getResult());
                    } else {
                        Assert.assertNull(response.getException());
                    }
                }
            });
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(new File(absolutePath + "/user.txt"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        postRequest.setHttpBody(new InputStreamBody(fis));
        liteHttp.executeAsync(postRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case16_7(){
        String uploadUrl1 = "/multipart";
        final StringRequest postRequest = new StringRequest(uploadUrl1)
            .setMethod(HttpMethods.Post)
            .setHttpListener(new HttpListener<String>(true, false, true) {
                @Override
                public void onStart(AbstractRequest<String> request) {
                    super.onStart(request);
                }
                @Override
                public void onUploading(AbstractRequest<String> request, long total, long len) {

                }
                @Override
                public void onEnd(Response<String> response) {
                    if (response.isConnectSuccess()) {
                        Assert.assertNotNull(response.getResult());
                    } else {
                        Assert.assertNull(response.getException());
                    }
                }
            });
        FileInputStream fis = null;
        String absolutePath2 = context.getExternalCacheDir().getPath();
        fis = null;
        try {
            fis = new FileInputStream(new File(absolutePath2 + "/2.jpg"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        MultipartBody body = new MultipartBody();
        body.addPart(new StringPart("key1", "hello"));
        body.addPart(new StringPart("key2", "很高兴见到你", "utf-8", null));
        body.addPart(new BytesPart("key3", new byte[]{1, 2, 3}));
        body.addPart(new FilePart("pic", new File(absolutePath2 + "/1.jpg"), "image/jpeg"));
        body.addPart(new InputStreamPart("litehttp", fis, "user.txt", "text/plain"));
        postRequest.setHttpBody(body);
        liteHttp.executeAsync(postRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case17(){
        // 17. Concurrent and Scheduling
        HttpConfig httpConfig = liteHttp.getConfig();
        // only one task can be executed at the same time
        httpConfig.setConcurrentSize(1);
        // at most two tasks be hold in waiting queue at the same time
        httpConfig.setWaitingQueueSize(2);
        // the last waiting task executed first
        httpConfig.setSchedulePolicy(SchedulePolicy.LastInFirstRun);
        // when task more than 3(current = 1, waiting = 2), new task will be discard.
        httpConfig.setOverloadPolicy(OverloadPolicy.DiscardCurrentTask);
        // note : restore config to default, next click.
        needRestore = true;
        x="";
        // by [DiscardCurrentTask Policy] the last will be discard.
        for (int i = 0; i < 4; i++) {
            liteHttp.executeAsync(new StringRequest(url).setTag(i).setHttpListener(
                new HttpListener<String>() {
                    @Override
                    public void onSuccess(String s, Response<String> response) {
                        setX((int)response.getRequest().getTag());
                        if(getX().length() == 3){
                            Assert.assertEquals("021", getX());
                        }
                    }
                }));
        }
        // submit order : 0 -> 1 -> 2 -> 3
        // task 0 is executing, 1 and 2 is in waitting queue, 3 was discarded.
        // real executed order: 0 -> 2 -> 1
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    String x = "";
    void setX(int x){
        this.x += x;
    }
    String getX(){
        return this.x;
    }
    @Test
    public void case18(){
        // 18. Detail of Configuration
        // init common headers for all request
        List<NameValuePair> headers = new ArrayList<NameValuePair>();
        headers.add(new NameValuePair("cookies", "this is cookies"));
        headers.add(new NameValuePair("custom-key", "custom-value"));
        HttpConfig newConfig = liteHttp.getConfig();
        // app context(be used to detect network and get app files path)
        newConfig.setContext(context);
        Assert.assertNotNull(newConfig.getContext());
        // the log is turn on when debugged is true
        newConfig.setDebugged(true);
        Assert.assertTrue(newConfig.isDebugged());
        // set user-agent
        newConfig.setUserAgent("Mozilla/5.0");
        Assert.assertEquals(newConfig.getUserAgent(), "Mozilla/5.0");
        // set global http listener to all request
        newConfig.setGlobalHttpListener(null);
        Assert.assertNull(newConfig.getGlobalHttpListener());
        // set global scheme and host to all request.
        newConfig.setBaseUrl("https://getman.cn");
        Assert.assertEquals(newConfig.getBaseUrl(), "https://getman.cn");
        // common headers will be set to all request
        newConfig.setCommonHeaders(headers);
        Assert.assertEquals(newConfig.getCommonHeaders(), headers);
        // set default cache path to all request
        newConfig.setDefaultCacheDir(Environment.DIRECTORY_DOWNLOADS + "/a-cache");
        Assert.assertEquals(newConfig.getDefaultCacheDir(), Environment.DIRECTORY_DOWNLOADS + "/a-cache");
        // set default cache expire time to all request
        newConfig.setDefaultCacheExpireMillis(30 * 60 * 1000);
        Assert.assertEquals(newConfig.getDefaultCacheExpireMillis(), 30 * 60 * 1000);
        // set default cache mode to all request
        newConfig.setDefaultCacheMode(CacheMode.NetFirst);
        Assert.assertEquals(newConfig.getDefaultCacheMode(), CacheMode.NetFirst);
        // set default charset to all request
        newConfig.setDefaultCharSet("utf-8");
        Assert.assertEquals(newConfig.getDefaultCharSet(), "utf-8");
        // set default http method to all request
        newConfig.setDefaultHttpMethod(HttpMethods.Get);
        Assert.assertEquals(newConfig.getDefaultHttpMethod(), HttpMethods.Get);
        // set default maximum redirect-times to all request
        newConfig.setDefaultMaxRedirectTimes(5);
        Assert.assertEquals(newConfig.getDefaultMaxRedirectTimes(), 5);
        // set default maximum retry-times to all request
        newConfig.setDefaultMaxRetryTimes(1);
        Assert.assertEquals(newConfig.getDefaultMaxRetryTimes(), 1);
        // set defsult model query builder to all request
        newConfig.setDefaultModelQueryBuilder(new JsonQueryBuilder());
        Assert.assertEquals(newConfig.getDefaultModelQueryBuilder(), new JsonQueryBuilder());
        // whether to detect network before conneting.
        newConfig.setDetectNetwork(true);
        Assert.assertEquals(newConfig.isDetectNetwork(), true);
        // disable some network
        newConfig.setDisableNetworkFlags(HttpConfig.FLAG_NET_DISABLE_NONE);
        Assert.assertEquals(newConfig.getDisableNetworkFlags(), HttpConfig.FLAG_NET_DISABLE_NONE);
        // whether open the traffic & time statistics
        newConfig.setDoStatistics(true);
        Assert.assertEquals(newConfig.isDoStatistics(), true);
        // set connect timeout: 10s
        newConfig.setConnectTimeout(10000);
        Assert.assertEquals(newConfig.getConnectTimeout(), 10000);
        // set socket timeout: 10s
        newConfig.setSocketTimeout(10000);
        Assert.assertEquals(newConfig.getSocketTimeout(), 10000);
        // if the network is unstable, wait 3000 milliseconds then start retry.
        newConfig.setRetrySleepMillis(3000);
        Assert.assertEquals(newConfig.getRetrySleepMillis(), 3000);
        // set maximum size of memory cache space
        newConfig.setMaxMemCacheBytesSize(1024 * 300);
        Assert.assertEquals(newConfig.getMaxMemCacheBytesSize(), 1024 * 300);
        // maximum number of concurrent tasks(http-request) at the same time
        newConfig.setConcurrentSize(3);
        Assert.assertEquals(newConfig.getConcurrentSize(), 3);
        // maximum number of waiting tasks(http-request) at the same time
        newConfig.setWaitingQueueSize(100);
        Assert.assertEquals(newConfig.getWaitingQueueSize(), 100);
        // set overload policy of thread pool executor
        newConfig.setOverloadPolicy(OverloadPolicy.DiscardOldTaskInQueue);
        Assert.assertEquals(newConfig.getOverloadPolicy(), OverloadPolicy.DiscardOldTaskInQueue);
        // set schedule policy of thread pool executor
        newConfig.setSchedulePolicy(SchedulePolicy.LastInFirstRun);
        Assert.assertEquals(newConfig.getSchedulePolicy(), SchedulePolicy.LastInFirstRun);
    }
    @Test
    public void case19(){
        // 19. Usage of Annotation
        @HttpUri(userUrl)
        @HttpMethod(HttpMethods.Get)
        @HttpID(1)
        @HttpCacheMode(CacheMode.CacheFirst)
        @HttpCacheExpire(value = 1, unit = TimeUnit.MINUTES)
        class UserAnnoParam implements HttpParamModel {
            public long id = 110;
            private String key = "aes";
        }
        liteHttp.executeAsync(new JsonRequest<User>(
            new UserAnnoParam(), User.class) {}.setHttpListener(
            new HttpListener<User>() {
                @Override
                public void onSuccess(User user, Response<User> response) {
                    Assert.assertNotNull(user);
                }
            }));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case20(){
        // 20. Multi Cache Mechanism
        StringRequest cacheRequest = new StringRequest(url);

        cacheRequest.setCacheMode(CacheMode.CacheFirst);
        cacheRequest.setCacheExpire(30, TimeUnit.SECONDS);
        cacheRequest.setCacheDir(absolutePath);
        cacheRequest.setCacheKey(null);

        cacheRequest.setHttpListener(new HttpListener<String>() {
            @Override
            public void onSuccess(String html, Response<String> response) {
            String title = response.isCacheHit() ? "Hit Cache(使用缓存)" : "No Cache(未用缓存)";
            Assert.assertNotNull(html);
            }
        });
        liteHttp.executeAsync(cacheRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case21(){
        // 21. CallBack Mechanism
        // the correct way to set global http listener for all request.
        liteHttp.getConfig().setGlobalHttpListener(globalHttpListener);
        /**
         * new http listener for current request:
         *
         * runOnUiThread = false;
         * readingNotify = false;
         * uploadingNotify = false;
         *
         * actually you can set a series of http listener for one http request.
         */
        HttpListener<PixelMap> firstHttpListener = new HttpListener<PixelMap>(false, false, false) {
            @Override
            public void onSuccess(PixelMap bitmap, Response<PixelMap> response) {
                Assert.assertNotNull(bitmap);
            }

            @Override
            public void onFailure(HttpException e, Response<PixelMap> response) {Assert.assertNull(e);}

            @Override
            public void onLoading(AbstractRequest<PixelMap> request, long total, long len) {}
        };
        // create a bitmap request.
        BitmapRequest bitmapRequest = new BitmapRequest(picUrl);
        // correct way to set first http listener
        bitmapRequest.setHttpListener(firstHttpListener);
        // correct way to set secondary (linked)listener
        firstHttpListener.setLinkedListener(secondaryListener);
        //load and show bitmap
        liteHttp.executeAsync(bitmapRequest);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case22(){
        // 22. Best Practices of SmartExecutor
        //可定义等待队列进入执行状态的策略：先来先执行，后来先执行。
        //可定义等待队列满载后处理新请求的策略：
        //- 抛弃队列中最新的任务
        //- 抛弃队列中最旧的任务
        //- 抛弃当前新任务
        //- 直接执行（阻塞当前线程）
        //- 抛出异常（中断当前线程）
        // 智能并发调度控制器：设置[最大并发数]，和[等待队列]大小
        SmartExecutor smallExecutor = new SmartExecutor();
        // set this temporary parameter, just for test
        // number of concurrent threads at the same time, recommended core size is CPU count
        smallExecutor.setCoreSize(2);
        // adjust maximum number of waiting queue size by yourself or based on phone performance
        smallExecutor.setQueueSize(2);
        // 任务数量超出[最大并发数]后，自动进入[等待队列]，等待当前执行任务完成后按策略进入执行状态：后进先执行。
        smallExecutor.setSchedulePolicy(SchedulePolicy.LastInFirstRun);
        // 后续添加新任务数量超出[等待队列]大小时，执行过载策略：抛弃队列内最旧任务。
        smallExecutor.setOverloadPolicy(OverloadPolicy.DiscardOldTaskInQueue);
        x="";
        // 一次投入 4 个任务
        for (int i = 0; i < 4; i++) {
            final int j = i;
            smallExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    setX(j);
                    Time.sleep(j * 200);
                }
            });
        }
        // 再投入1个需要取消的任务
        Future future = smallExecutor.submit(new Runnable() {
            @Override
            public void run() {
                setX(4);
                Time.sleep(1000);
            }
        });
        future.cancel(false);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean ass = "013".equals(getX()) || "103".equals(getX());
        Assert.assertTrue(ass);
    }
    @Test
    public void case23(){
        // 23. Automatic Conversion of Complex Model
        // 模拟用户登录
        liteHttp.executeAsync(new LoginParam("123", "456").setHttpListener(
            new HttpListener<User>() {
                @Override
                public void onSuccess(User user, Response<User> response) {
                    Assert.assertNotNull(user);
                }

                @Override
                public void onFailure(HttpException e, Response<User> response) {
                    Assert.assertNull(e);
                }
            }
        ));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void case24_1(){
        // 24. Best Practice: HTTP Rich Param Model (It is simpler and More Useful)
        // rich param 更简单、有用！只需要定义一个RichParam，可指定URL、参数、返回响应体三个关键事物。
        // request : http://litesuits.com/mockdata/user_get?id=110&key=aes-125
        // response: User
        @HttpUri("{url}/{path}")
        class UserRichParam extends HttpRichParamModel<User> {
            @NonHttpParam
            @HttpReplace("url")
            private String url = "http://192.168.1.156:8080";
            @NonHttpParam
            @HttpReplace("path")
            private String path = "user_get";
            public long id = 110;
            private String key = "aes-125";
        }
        // 一句话调用即可
        JsonRequest<User> userJsonRequest = liteHttp.executeAsync(new UserRichParam());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(userJsonRequest);
    }
    @Test
    public void case24_2(){
        // 其他更多注解还有：
        @HttpBaseUrl("{host}") // 定义scheme，使用host变量的值取代
        @HttpUri("{path}") // 定义uri 或者 path
        @HttpMethod(HttpMethods.Get) // 请求方式
        @HttpCharSet("UTF-8") // 请求编码
        @HttpTag("custom tag") // 打TAG
        @HttpCacheMode(CacheMode.CacheFirst) // 缓存模式
        @HttpCacheKey("custom-cache-key-name-by-myself") // 缓存文件名字
        @HttpCacheExpire(value = 1, unit = TimeUnit.MINUTES) // 缓存时间
        @HttpID(2) // 请求ID
        @HttpMaxRetry(3) // 重试次数
        @HttpMaxRedirect(5)
        // 重定向次数
        class TEST extends HttpRichParamModel<User> {
            @NonHttpParam
            @HttpReplace("host")
            private String host = "http://192.168.1.156:8080";
            @NonHttpParam
            @HttpReplace("path")
            private String path = "/user_get";
            // 可以复写设置headers/attachToUrl/listener/httpbody 等参数
            /**
             * 返回true则将将成员变量{@link #host}、{@link #path}拼接到url中
             * 返回false，则不拼接。
             * @return
             */
            @Override
            public boolean isFieldsAttachToUrl() {
                return false;
            }
            @Override
            protected LinkedHashMap<String, String> createHeaders() {
                return super.createHeaders();
            }
            @Override
            protected HttpListener<User> createHttpListener() {
                return super.createHttpListener();
            }
            @Override
            protected HttpBody createHttpBody() {
                return super.createHttpBody();
            }
        }
        liteHttp.executeAsync(new TEST().setHttpListener(new HttpListener<User>() {
            @Override
            public void onSuccess(User user, Response<User> response) {
                Assert.assertNotNull(user);
            }
            @Override
            public void onFailure(HttpException e, Response<User> response) {
                Assert.assertNull(e);
            }
        }));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initLiteHttp(String baseUrl) {

        if (liteHttp == null) {
            liteHttp = LiteHttp.build(context)
                    .setHttpClient(new HttpUrlClient())       // http client
                    .setJsonConvertor(new GsonImpl())        // json convertor
                    .setBaseUrl(baseUrl)                    // set base url
                    .setDebugged(true)                     // log output when debugged
                    .setDoStatistics(true)                // statistics of time and traffic
                    .setDetectNetwork(true)              // detect network before connect
                    .setUserAgent("Mozilla/5.0 (...)")  // set custom User-Agent
                    .setSocketTimeout(10000)           // socket timeout: 10s
                    .setConnectTimeout(10000)         // connect timeout: 10s
                    .create();
        } else {
            liteHttp.getConfig()                   // configuration directly
                    .setSocketTimeout(5000)       // socket timeout: 5s
                    .setConnectTimeout(5000);    // connect timeout: 5s
            HttpUrlClient url = new HttpUrlClient();
            //url.setHostnameVerifier()
        }
    }
    // 实现登陆，参数为 name 和 password，成功后返回 User 对象。
    @HttpUri(loginUrl)
    @HttpMethod(HttpMethods.Post)
    class LoginParam extends HttpRichParamModel<User> {
        private String name;
        private String password;

        public LoginParam(String name, String password) {
            this.name = name;
            this.password = password;
        }
    }

    /**
     * global http listener for all request.
     */
    GlobalHttpListener globalHttpListener = new GlobalHttpListener() {
        @Override
        public void onStart(AbstractRequest<?> request) {
            HttpLog.i("", "Global, request start ...");
        }

        @Override
        public void onSuccess(Object data, Response<?> response) {
            Assert.assertNotNull(data);
        }

        @Override
        public void onFailure(HttpException e, Response<?> response) {
            Assert.assertNull(e);
        }

        @Override
        public void onCancel(Object data, Response<?> response) {
            HttpLog.i("", "Global, request cancel ..." + data);
        }
    };
    /**
     * http listener for current reuqest:
     *
     * runOnUiThread = true;
     * readingNotify = true;
     * uploadingNotify = true;
     */
    HttpListener<PixelMap> secondaryListener = new HttpListener<PixelMap>(true, true, true) {
        ProgressBar progressDialog = null;
        long preTotal = 0;
        long preLen = 0;
        @Override
        public void onStart(AbstractRequest<PixelMap> request) {
            HttpLog.i("", "second listener, request start ...");
        }
        @Override
        public void onSuccess(PixelMap bitmap, Response<PixelMap> response) {
            Assert.assertNotNull(bitmap);
        }
        @Override
        public void onFailure(HttpException e, Response<PixelMap> response) {
            Assert.assertNull(e);
        }
        @Override
        public void onLoading(AbstractRequest<PixelMap> request, long total, long len) {
            preTotal = total;
            preLen = len;
        }
    };

}