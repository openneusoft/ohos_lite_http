package com.litesuits.http;

import com.litesuits.http.exception.HttpException;
import com.litesuits.http.listener.HttpListener;
import com.litesuits.http.request.StringRequest;
import com.litesuits.http.request.param.HttpMethods;
import com.litesuits.http.response.Response;
import org.junit.Test;

import static org.junit.Assert.*;

public class LiteHttpTest {

    private boolean needRestore;
    protected static LiteHttp liteHttp;
    @Test
    public void executeAsync() {
        String baseUrl = "https://www.baidu.com";
        if (needRestore) {
            liteHttp.getConfig().restoreToDefault().setBaseUrl(baseUrl);
            needRestore = false;
        }
        String apiUrl = "http://api.daily.taobao.net/router/rest?app_key=4272&format=json&method=cainiao.guoguo.courier.getrewardtip&open_id=4398046618004&session_code=7949690a30a91305a3a351a314ec501e&sign=0A9DDED897FDB04E1D8177C984AD4238&sign_method=md5&timestamp=2016-08-05&user_id=4398046618004&v=2.0";
        StringRequest login = new StringRequest(apiUrl)
                .setMethod(HttpMethods.Get)
                .setHttpListener(new HttpListener<String>() {
                    @Override
                    public void onSuccess(String s, Response<String> response) {
                        assertEquals(response.getHttpStatus(), "200");
                    }

                    @Override
                    public void onFailure(HttpException e, Response<String> response) {
                        assertEquals(response.getHttpStatus(), "200");
                    }
                });
        liteHttp.executeAsync(login);
    }
}