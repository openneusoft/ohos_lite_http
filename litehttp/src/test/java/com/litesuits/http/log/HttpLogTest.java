package com.litesuits.http.log;

import org.junit.Test;

import static org.junit.Assert.*;

public class HttpLogTest {

    @Test
    public void i() {
        HttpLog.i("TEST", new Integer(1));
    }

    @Test
    public void v() {
        HttpLog.v("TEST", "Test Throwable.", new Throwable("New a exception"));
    }

    @Test
    public void d() {
        HttpLog.d("TEST", "Test Throwable.", new Throwable("New a exception"));
    }

    @Test
    public void testI() {
        HttpLog.i("TEST", "Test Throwable.", new Throwable("New a exception"));
    }

    @Test
    public void e() {
        HttpLog.e("TEST", "Test Throwable.", new Throwable("New a exception"));
    }
}