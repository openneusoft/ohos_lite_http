package com.litesuits.http.network;

import ohos.app.Context;
import ohos.net.NetCapabilities;
import ohos.net.NetHandle;
import com.litesuits.http.log.HttpLog;

import ohos.net.NetManager;


public class Network {
	private static final String TAG = Network.class.getSimpleName();

	public enum NetType {
		//Unknown(0),
		None(1),
		Mobile(2),
		Wifi(4),
		Other(8);
		NetType(int value) {
			this.value = value;
		}
		public int value;
	}

	/**
	 * 获取ConnectivityManager
	 */
	public static NetManager getConnManager(Context context) {
		return NetManager.getInstance(context);
	}

	/**
	 * 判断网络连接是否有效（此时可传输数据）。
	 * @param context
	 * @return boolean 不管wifi，还是mobile net，只有当前在连接状态（可有效传输数据）才返回true,反之false。
	 */
	public static boolean isConnected(Context context) {
		NetManager netManager = getConnManager(context);
		NetHandle[] allNets = netManager.getAllNets();
		return allNets != null && allNets.length > 0;
	}

	/**
	 * 判断有无网络正在连接中（查找网络、校验、获取IP等）。
	 * @param context
	 * @return boolean 不管wifi，还是mobile net，只有当前在连接状态（可有效传输数据）才返回true,反之false。
	 */
	public static boolean isConnectedOrConnecting(Context context) {
		NetManager netManager = getConnManager(context);
		NetHandle[] nets = netManager.getAllNets();
		if (nets != null) {
			for (NetHandle net : nets) {
				//TODO:Connecting()
				boolean isConnectedOrConnecting = netManager.sendNetConnectState(net, true);
				if (isConnectedOrConnecting) {
					return true;
				}
			}
		}
		return false;
	}

	public static NetType getConnectedType(Context context) {
		NetManager netManager = getConnManager(context);
		NetHandle net = netManager.getDefaultNet();
		if (net != null) {
			NetCapabilities netCapabilities = netManager.getNetCapabilities(net);

			Boolean wifiType = netCapabilities.hasBearer(NetCapabilities.BEARER_WIFI);
			Boolean cellularType = netCapabilities.hasBearer(NetCapabilities.BEARER_CELLULAR);

			if (wifiType) {
				return NetType.Wifi;
			} else if (cellularType) {
				return NetType.Mobile;
			} else {
				return NetType.Other;
			}
		}
		return NetType.None;
	}

	/**
	 * 是否存在有效的WIFI连接
	 */
	public static boolean isWifiConnected(Context context) {
		NetManager netManager = getConnManager(context);
		NetHandle net = netManager.getDefaultNet();
		if (net != null) {
			NetCapabilities netCapabilities = netManager.getNetCapabilities(net);
			return netCapabilities.hasBearer(NetCapabilities.BEARER_WIFI) ||
					netCapabilities.hasBearer(NetCapabilities.BEARER_WIFI_AWARE);
		}
		return false;
	}

	/**
	 * 是否存在有效的移动连接
	 * @param context
	 * @return boolean
	 */
	public static boolean isMobileConnected(Context context) {
		NetManager netManager = getConnManager(context);
		NetHandle net = netManager.getDefaultNet();
		if (net != null) {
			NetCapabilities netCapabilities = netManager.getNetCapabilities(net);
			return netCapabilities.hasBearer(NetCapabilities.BEARER_CELLULAR);
		}
		return false;
	}

	/**
	 * 检测网络是否为可用状态
	 */
	public static boolean isAvailable(Context context) {
		return isWifiAvailable(context) || (isMobileAvailable(context));
	}

	/**
	 * 判断是否有可用状态的Wifi，以下情况返回false：
	 *  1. 设备wifi开关关掉;
	 *  2. 已经打开飞行模式；
	 *  3. 设备所在区域没有信号覆盖；
	 *  4. 设备在漫游区域，且关闭了网络漫游。
	 *
	 * @param context
	 * @return boolean wifi为可用状态（不一定成功连接，即Connected）即返回ture
	 */
	public static boolean isWifiAvailable(Context context) {
		NetManager netManager = getConnManager(context);
		NetHandle[] nets = netManager.getAllNets();
		if (nets != null) {
			for (NetHandle net : nets) {
				NetCapabilities netCapabilities = netManager.getNetCapabilities(net);
				if (netCapabilities.hasBearer(NetCapabilities.BEARER_WIFI) || netCapabilities.hasBearer(NetCapabilities.BEARER_WIFI_AWARE)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 判断有无可用状态的移动网络，注意关掉设备移动网络直接不影响此函数。
	 * 也就是即使关掉移动网络，那么移动网络也可能是可用的(彩信等服务)，即返回true。
	 * 以下情况它是不可用的，将返回false：
	 *  1. 设备打开飞行模式；
	 *  2. 设备所在区域没有信号覆盖；
	 *  3. 设备在漫游区域，且关闭了网络漫游。
	 *
	 * @param context
	 * @return boolean
	 */
	public static boolean isMobileAvailable(Context context) {
		NetManager netManager = getConnManager(context);
		NetHandle[] nets = netManager.getAllNets();
		if (nets != null) {
			for (NetHandle net : nets) {
				NetCapabilities netCapabilities = netManager.getNetCapabilities(net);
				if (netCapabilities.hasBearer(NetCapabilities.BEARER_CELLULAR) || netCapabilities.hasBearer(NetCapabilities.NET_CAPABILITY_MMS)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 设备是否打开移动网络开关
	 * @param context
	 * @return boolean 打开移动网络返回true，反之false
	 */
//	public static boolean isMobileEnabled(Context context) {
//		try {
//			Method getMobileDataEnabledMethod = ConnectivityManager.class.getDeclaredMethod("getMobileDataEnabled");
//			getMobileDataEnabledMethod.setAccessible(true);
//			return (Boolean) getMobileDataEnabledMethod.invoke(getConnManager(context));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		// 反射失败，默认开启
//		return true;
//	}

	/**
	 * 打印当前各种网络状态
	 * @param context
	 * @return boolean
	 */
	public static boolean printNetworkInfo(Context context) {
		NetManager netManager = getConnManager(context);
		if (netManager != null) {
			//NetworkInfo in = connectivity.getActiveNetworkInfo();
			NetHandle in = netManager.getDefaultNet();
			HttpLog.i(TAG, "-------------$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$-------------");
			HttpLog.i(TAG, "getDefaultNet: " + in);
			//NetworkInfo[] info = connectivity.getAllNetworkInfo();
			NetHandle[] info = netManager.getAllNets();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					NetCapabilities netCapabilities = netManager.getNetCapabilities(info[i]);

					if (netCapabilities.hasCap(NetCapabilities.NET_CAPABILITY_VALIDATED)) {
						// 网络连接成功，并且可以上网
						if (netCapabilities.hasBearer(NetCapabilities.BEARER_WIFI)||
								netCapabilities.hasBearer(NetCapabilities.BEARER_WIFI_AWARE)) {
							HttpLog.i(TAG, "NetInfo[" + i + "] is wifi and Available");
						} else if (netCapabilities.hasBearer(NetCapabilities.BEARER_CELLULAR) ||
								netCapabilities.hasBearer(NetCapabilities.BEARER_ETHERNET)) {
							HttpLog.i(TAG, "NetInfo[" + i + "] is cellular and Available");
						} else {
							// 其它网络，包括蓝牙、VPN、LoWPAN等
							HttpLog.i(TAG, "NetInfo[" + i + "] is other net.");
						}
					}
				}
				HttpLog.i(TAG, "\n");
			} else {
				HttpLog.i(TAG, "getAllNets is null");
			}
		}
		return false;
	}

}