package com.litesuits.http.parser.impl;

import ohos.media.image.PixelMap;
import ohos.media.image.ImageSource;
import com.litesuits.http.parser.FileCacheableParser;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * parse inputstream to bitmap.
 *
 * @author MaTianyu
 *         2014-2-21下午8:56:59
 */
public class BitmapParser extends FileCacheableParser<PixelMap> {
    public BitmapParser() {}
    public BitmapParser(File file) {
        this.file = file;
    }

    @Override
    public PixelMap parseNetStream(InputStream stream, long len, String charSet) throws IOException {
        //file = streamToFile(stream, len);
        ImageSource imageSource = ImageSource.create(stream, new ImageSource.SourceOptions());
        PixelMap pixelmap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
        return pixelmap;
        //return BitmapFactory.decodeFile(file.getAbsolutePath());
    }

    @Override
    public PixelMap parseDiskCache(File file) {
        ImageSource imageSource = ImageSource.create(file, new ImageSource.SourceOptions());
        return imageSource.createPixelmap(new ImageSource.DecodingOptions());
        // return BitmapFactory.decodeFile(file.getAbsolutePath());
    }
}