package com.litesuits.http.listener;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import com.litesuits.http.exception.HttpException;
import com.litesuits.http.log.HttpLog;
import com.litesuits.http.request.AbstractRequest;
import com.litesuits.http.response.Response;

/**
 * @author MaTianyu
 * @date 2014-11-06
 */
public abstract class GlobalHttpListener {
    private static final String TAG = GlobalHttpListener.class.getSimpleName();

    private static final int M_START = 1;
    private static final int M_SUCCESS = 2;
    private static final int M_FAILURE = 3;
    private static final int M_CANCEL = 4;

    private HttpHandler handler;
    private boolean runOnUiThread = true;

    /**
     * default run on UI thread
     */
    public GlobalHttpListener() {
        this(true);
    }

    public GlobalHttpListener(boolean runOnUiThread) {
        setRunOnUiThread(runOnUiThread);
    }

    public boolean isRunOnUiThread() {
        return runOnUiThread;
    }

    public GlobalHttpListener setRunOnUiThread(boolean runOnUiThread) {
        this.runOnUiThread = runOnUiThread;
        if (runOnUiThread) {
            handler = new HttpHandler(EventRunner.getMainEventRunner());
        } else {
            handler = null;
        }
        return this;
    }

    /**
     * note: hold an implicit reference to outter class
     */
    private class HttpHandler extends EventHandler {
        private HttpHandler(EventRunner runner) {
            super(runner);
        }

        @Override
        public void processEvent(InnerEvent event) {
            Object[] data;
            switch (event.eventId) {
                case M_START:
                    onStart((AbstractRequest<Object>) event.object);
                    break;
                case M_SUCCESS:
                    data = (Object[]) event.object;
                    onSuccess(data[0], (Response<Object>) data[1]);
                    break;
                case M_FAILURE:
                    data = (Object[]) event.object;
                    onFailure((HttpException) data[0], (Response<Object>) data[1]);
                    break;
                case M_CANCEL:
                    data = (Object[]) event.object;
                    onCancel(data[0], (Response<Object>) data[1]);
                    break;
            }
        }
    }

    //____________lite called method ____________
    public final void notifyCallStart(AbstractRequest<?> req) {
        if (runOnUiThread) {
//          InnerEvent event =  handler.obtainMessage(M_START);
            InnerEvent event = InnerEvent.get(M_START);
            event.object = req;
            handler.sendEvent(event);
        } else {
            onStart(req);
        }
    }

    public final void notifyCallSuccess(Object data, Response<?> response) {
        if (runOnUiThread) {
            //Message msg = handler.obtainMessage(M_SUCCESS);
            InnerEvent event = InnerEvent.get(M_SUCCESS);
            event.object = new Object[]{data, response};
            handler.sendEvent(event);
        } else {
            onSuccess(data, response);
        }
    }

    public final void notifyCallFailure(HttpException e, Response<?> response) {
        if (runOnUiThread) {
            //Message msg = handler.obtainMessage(M_FAILURE);
            InnerEvent event = InnerEvent.get(M_FAILURE);
            event.object = new Object[]{e, response};
            handler.sendEvent(event);
        } else {
            onFailure(e, response);
        }
    }

    public final void notifyCallCancel(Object data, Response<?> response) {
        if (HttpLog.isPrint) {
            HttpLog.w(TAG, "Request be Cancelled!  isCancelled: " + response.getRequest().isCancelled()
                           + "  Thread isInterrupted: " + Thread.currentThread().isInterrupted());
        }
        if (runOnUiThread) {
            //Message msg = handler.obtainMessage(M_CANCEL);
            InnerEvent event = InnerEvent.get(M_CANCEL);
            event.object = new Object[]{data, response};
            handler.sendEvent(event);
        } else {
            onCancel(data, response);
        }
    }

    //____________ developer override method ____________
    public void onStart(AbstractRequest<?> request){}

    public abstract void onSuccess(Object data, Response<?> response);

    public abstract void onFailure(HttpException e, Response<?> response);

    public void onCancel(Object data, Response<?> response){}

}
