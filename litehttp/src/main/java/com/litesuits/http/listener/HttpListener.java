package com.litesuits.http.listener;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import com.litesuits.http.exception.HttpException;
import com.litesuits.http.log.HttpLog;
import com.litesuits.http.request.AbstractRequest;
import com.litesuits.http.response.Response;

/**
 * @author MaTianyu
 * @date 2014-11-06
 */
public abstract class HttpListener<Data> {
    private static final String TAG = HttpListener.class.getSimpleName();

    private static final int M_START = 1;
    private static final int M_SUCCESS = 2;
    private static final int M_FAILURE = 3;
    private static final int M_CANCEL = 4;
    private static final int M_READING = 5;
    private static final int M_UPLOADING = 6;
    private static final int M_RETRY = 7;
    private static final int M_REDIRECT = 8;
    private static final int M_END = 9;

    private HttpHandler handler;
    private boolean runOnUiThread = true;
    private boolean readingNotify = false;
    private boolean uploadingNotify = false;
    private HttpListener<Data> linkedListener;
    private long delayMillis;

    /**
     * default run on UI thread
     */
    public HttpListener() {
        this(true);
    }

    public HttpListener(long delayMillis) {
        this.delayMillis = delayMillis;
    }

    public HttpListener(boolean runOnUiThread) {
        setRunOnUiThread(runOnUiThread);
    }

    public HttpListener(boolean runOnUiThread, boolean readingNotify, boolean uploadingNotify) {
        this(runOnUiThread);
        this.readingNotify = readingNotify;
        this.uploadingNotify = uploadingNotify;
    }

    public final HttpListener<Data> getLinkedListener() {
        return linkedListener;
    }

    public final HttpListener<Data> setLinkedListener(HttpListener<Data> httpListener) {
        if (this.linkedListener != null) {
            HttpListener<Data> temp = this.linkedListener;
            do {
                if (httpListener == temp) {
                    throw new RuntimeException("Circular refrence:  " + httpListener);
                }
            } while ((temp = temp.getLinkedListener()) != null);
        }
        this.linkedListener = httpListener;
        return this;
    }

    public final boolean isRunOnUiThread() {
        return runOnUiThread;
    }

    public final HttpListener<Data> setRunOnUiThread(boolean runOnUiThread) {
        this.runOnUiThread = runOnUiThread;
        if (runOnUiThread) {
            handler = new HttpHandler(EventRunner.getMainEventRunner());
        } else {
            handler = null;
        }
        return this;
    }

    public final boolean isReadingNotify() {
        return readingNotify;
    }

    public final HttpListener<Data> setReadingNotify(boolean readingNotify) {
        this.readingNotify = readingNotify;
        return this;
    }

    public final boolean isUploadingNotify() {
        return uploadingNotify;
    }

    public final HttpListener<Data> setUploadingNotify(boolean uploadingNotify) {
        this.uploadingNotify = uploadingNotify;
        return this;
    }

    public long getDelayMillis() {
        return delayMillis;
    }

    public HttpListener<Data> setDelayMillis(long delayMillis) {
        this.delayMillis = delayMillis;
        return this;
    }

    /**
     * note: hold an implicit reference to outter class
     */
    private class HttpHandler extends EventHandler {
        private HttpHandler(EventRunner runner) {
            super(runner);
        }

        @Override
        public void processEvent(InnerEvent event) {
            Object[] data;
            switch (event.eventId) {
                case M_START://OK
                    onStart((AbstractRequest<Data>) event.object);
                    break;
                case M_SUCCESS://OK
                    data = (Object[]) event.object;
                    onSuccess((Data)data[0], (Response<Data>) data[1]);
                    break;
                case M_FAILURE://OK
                    data = (Object[]) event.object;
                    onFailure((HttpException) data[0], (Response<Data>) data[1]);
                    break;
                case M_CANCEL://OK
                    data = (Object[]) event.object;
                    onCancel((Data)data[0], (Response<Data>) data[1]);
                    break;
                case M_READING:
                    data = (Object[]) event.object;
                    onLoading((AbstractRequest<Data>) data[0], (Long) data[1], (Long) data[2]);
                    break;
                case M_UPLOADING:
                    data = (Object[]) event.object;
                    onUploading((AbstractRequest<Data>) data[0], (Long) data[1], (Long) data[2]);
                    break;
                case M_RETRY://OK
                    data = (Object[]) event.object;
                    onRetry((AbstractRequest<Data>) data[0], (Integer) data[1], (Integer) data[2]);
                    break;
                case M_REDIRECT://OK
                    data = (Object[]) event.object;
                    onRedirect((AbstractRequest<Data>) data[0], (Integer) data[1], (Integer) data[2]);
                    break;
                case M_END://OK
                    onEnd((Response<Data>) event.object);
                    break;
            }
        }
    }

    //____________lite called method ____________
    public final void notifyCallStart(AbstractRequest<Data> req) {
        if (disableListener()) {
            return;
        }
        if (runOnUiThread) {
            InnerEvent event = InnerEvent.get(M_START);
            event.object = req;
            handler.sendEvent(event);
        } else {
            onStart(req);
        }
        if (linkedListener != null) {
            linkedListener.notifyCallStart(req);
        }
    }

    public final void notifyCallSuccess(Data data, Response<Data> response) {
        delayOrNot();
        if (disableListener()) {
            return;
        }
        if (runOnUiThread) {
            InnerEvent event = InnerEvent.get(M_SUCCESS);
            event.object = new Object[]{data, response};
            handler.sendEvent(event);
        } else {
            onSuccess(data, response);
        }
        if (linkedListener != null) {
            linkedListener.notifyCallSuccess(data, response);
        }
    }

    public final void notifyCallFailure(HttpException e, Response<Data> response) {
        delayOrNot();
        if (disableListener()) {
            return;
        }
        if (runOnUiThread) {
            InnerEvent event = InnerEvent.get(M_FAILURE);
            event.object = new Object[]{e, response};
            handler.sendEvent(event);
        } else {
            onFailure(e, response);
        }
        if (linkedListener != null) {
            linkedListener.notifyCallFailure(e, response);
        }
    }

    public final void notifyCallCancel(Data data, Response<Data> response) {
        if (HttpLog.isPrint) {
            HttpLog.w(TAG, "Request be Cancelled!  isCancelled: " + response.getRequest().isCancelled()
                           + "  Thread isInterrupted: " +  Thread.currentThread().isInterrupted());
        }
        delayOrNot();
        if (disableListener()) {
            return;
        }
        if (runOnUiThread) {
            InnerEvent event = InnerEvent.get(M_CANCEL);
            event.object = new Object[]{data, response};
            handler.sendEvent(event);
        } else {
            onCancel(data, response);
        }
        if (linkedListener != null) {
            linkedListener.notifyCallCancel(data, response);
        }
    }

    public final void notifyCallLoading(AbstractRequest<Data> req, long total, long len) {
        if (disableListener()) {
            return;
        }
        if (readingNotify) {
            if (runOnUiThread) {
                InnerEvent event = InnerEvent.get(M_READING);
                event.object = new Object[]{req, total, len};
                handler.sendEvent(event);
            } else {
                onLoading(req, total, len);
            }
        }
        if (linkedListener != null) {
            linkedListener.notifyCallLoading(req, total, len);
        }
    }

    public final void notifyCallUploading(AbstractRequest<Data> req, long total, long len) {
        if (disableListener()) {
            return;
        }
        if (uploadingNotify) {
            if (runOnUiThread) {
                InnerEvent event = InnerEvent.get(M_UPLOADING);
                event.object = new Object[]{req, total, len};
                handler.sendEvent(event);
            } else {
                onUploading(req, total, len);
            }
        }
        if (linkedListener != null) {
            linkedListener.notifyCallUploading(req, total, len);
        }
    }

    public final void notifyCallRetry(AbstractRequest<Data> req, int max, int times) {
        if (disableListener()) {
            return;
        }
        if (runOnUiThread) {
            InnerEvent event = InnerEvent.get(M_RETRY);
            event.object = new Object[]{req, max, times};
            handler.sendEvent(event);
        } else {
            onRetry(req, max, times);
        }
        if (linkedListener != null) {
            linkedListener.notifyCallRetry(req, max, times);
        }
    }

    public final void notifyCallRedirect(AbstractRequest<Data> req, int max, int times) {
        if (disableListener()) {
            return;
        }
        if (runOnUiThread) {
            InnerEvent event = InnerEvent.get(M_REDIRECT);
            event.object = new Object[]{req, max, times};
            handler.sendEvent(event);
        } else {
            onRedirect(req, max, times);
        }
        if (linkedListener != null) {
            linkedListener.notifyCallRedirect(req, max, times);
        }
    }

    public final void notifyCallEnd(Response<Data> response) {
        if (disableListener()) {
            return;
        }
        if (runOnUiThread) {
            InnerEvent event = InnerEvent.get(M_END);
            event.object = response;
            handler.sendEvent(event);
        } else {
            onEnd(response);
        }
        if (linkedListener != null) {
            linkedListener.notifyCallEnd(response);
        }
    }

    private boolean delayOrNot() {
        if (delayMillis > 0) {
            try {
                Thread.sleep(delayMillis);
                return true;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    //____________ developer override method ____________
    public boolean disableListener() {
        return false;
    }

    public void onStart(AbstractRequest<Data> request) {}

    public void onSuccess(Data data, Response<Data> response) {}

    public void onFailure(HttpException e, Response<Data> response) {}

    public void onCancel(Data data, Response<Data> response) {}

    public void onLoading(AbstractRequest<Data> request, long total, long len) {}

    public void onUploading(AbstractRequest<Data> request, long total, long len) {}

    public void onRetry(AbstractRequest<Data> request, int max, int times) {}

    public void onRedirect(AbstractRequest<Data> request, int max, int times) {}

    public void onEnd(Response<Data> response) {}
}
