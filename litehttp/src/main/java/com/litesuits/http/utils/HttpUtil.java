package com.litesuits.http.utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import com.litesuits.http.log.HttpLog;
import com.litesuits.http.request.param.HttpRichParamModel;

import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * @author MaTianyu
 * @date 2015-04-21
 */
public class HttpUtil {
    private static final String TAG = HttpUtil.class.getSimpleName();

    private static final String PATH_CPU = "/sys/devices/system/cpu/";
    private static final String CPU_FILTER = "cpu[0-9]+";
    private static int CPU_CORES = 0;

    public static String formatDate(long millis) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(new Date(millis));
    }

    /**
     * Get available processors.
     */
    public static int getProcessorsCount() {
        return Runtime.getRuntime().availableProcessors();
    }

    /**
     * Gets the number of cores available in this device, across all processors.
     * Requires: Ability to peruse the filesystem at "/sys/devices/system/cpu"
     *
     * @return The number of cores, or available processors if failed to get result
     */
    public static int getCoresNumbers() {
        if (CPU_CORES > 0) {
            return CPU_CORES;
        }
        //Private Class to display only CPU devices in the directory listing
        class CpuFilter implements FileFilter {
            @Override
            public boolean accept(File pathname) {
                //Check if filename is "cpu", followed by a single digit number
                if (Pattern.matches(CPU_FILTER, pathname.getName())) {
                    return true;
                }
                return false;
            }
        }
        try {
            //Get directory containing CPU info
            File dir = new File(PATH_CPU);
            //Filter to only list the devices we care about
            File[] files = dir.listFiles(new CpuFilter());
            //Return the number of cores (virtual CPU devices)
            CPU_CORES = files.length;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (CPU_CORES < 1) {
            CPU_CORES = Runtime.getRuntime().availableProcessors();
        }
        if (CPU_CORES < 1) {
            CPU_CORES = 1;
        }
        HttpLog.i(TAG, "CPU cores: " + CPU_CORES);
        return CPU_CORES;
    }

    public static CommonDialog dialogBuilder(Context context, String title, String msg) {
        CommonDialog builder = new CommonDialog(context);

        if (msg != null) {
            builder.setContentText(msg);
        }
        if (title != null) {
            builder.setTitleText(title);
        }
        return builder;
    }


    public static ToastDialog showTips(Context context, String title, String des) {
        return showTips(context, title, des, null, null);
    }

    public static ToastDialog showTips(Context context, String title, String des, String btn,
                                       CommonDialog.DestroyedListener destroyedListener) {
        ToastDialog dialog = new ToastDialog(context);
        dialog.setText(des);
        dialog.setSize(1000,1200);
        dialog.setDuration(2000);
        dialog.setAlignment(LayoutAlignment.CENTER);
        dialog.setDestroyedListener(destroyedListener);
        dialog.setTitleText(title);

        Text txt = (Text) dialog.getComponent();
        txt.setMultipleLine(true);
        txt.setWidth(DirectionalLayout.LayoutConfig.MATCH_PARENT);
        txt.setHeight(DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        ShapeElement toastBackground = new ShapeElement();
        toastBackground.setRgbColor(new RgbColor(255,211,155));
        txt.setBackground(toastBackground);

        dialog.show();
        return dialog;
    }

    public static ArrayList<Field> getAllParamModelFields(Class<?> claxx) {
        // find all field.
        ArrayList<Field> fieldList = new ArrayList<Field>();
        while (claxx != null && claxx != HttpRichParamModel.class && claxx != Object.class) {
            Field[] fs = claxx.getDeclaredFields();
            for (int i = 0; i < fs.length; i++) {
                Field f = fs[i];
                if (!f.isSynthetic()) {
                    fieldList.add(f);
                }
            }
            claxx = claxx.getSuperclass();
        }
        return fieldList;
    }
}
