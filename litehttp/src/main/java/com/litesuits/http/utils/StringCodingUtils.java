package com.litesuits.http.utils;


import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/**
 * @author MaTianyu
 * @date 2014-12-05
 */
public class StringCodingUtils {

    public static byte[] getBytes(String src, Charset charSet) {
        int Build_VERSION_SDK_INT = 0;
        int Build_VERSION_CODES_GINGERBREAD = 9;
        // Build.VERSION_CODES.GINGERBREAD = 9
        if (Build_VERSION_SDK_INT < Build_VERSION_CODES_GINGERBREAD) {
            try {
                return src.getBytes(charSet.name());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return src.getBytes(charSet);
        }
    }

}
